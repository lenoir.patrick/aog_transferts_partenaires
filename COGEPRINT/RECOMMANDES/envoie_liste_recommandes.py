# coding: utf-8
import json
import os
import pandas as pd
from rich.progress import Progress, TimeElapsedColumn, SpinnerColumn
import shutil
import zipfile

import AOG.tools as aogtools

with open(os.path.join(os.path.dirname(__file__), "..", 'appsettings.json')) as json_data:
    config = json.load(json_data)

# Constantes texte
NEW_CSV = "_new.csv"
ET_MAJ = "Mise à jour"
ET_EXPEDIE = "Expédié le"
ET_DATE_DIFF = "Date Diff"
ET_NUM_RECO = "  N° Recommandé"
RECUPERATION_DE = "        Récupération de : "

DATE_FORMAT = "%d/%m/%y"


if __name__ == "__main__":
    appname = "TRAITEMENT_LISTES_LR"
    gc = aogtools.GeneralConfig(appname)

    dirretourpdf = config["recommandes"]["dirretourpdf"]

    # On cherche les cas spécifiques pour les partenaires
    for item in config["recommandes"]["retour_recommandes"]["clients"]:
        compagnie = item["nom"]
        inclusion = item["retour_liste"]["inclusion"]
        iszip = item["retour_liste"]["zip"]
        working_dir = item["retour_liste"]["working_dir"]
        specif = item["retour_liste"]["specif"]

        input_dir = os.path.join(working_dir, "input")

        archive_dir = os.path.join(working_dir, "archive")
        if os.path.isdir(archive_dir) is False:
            os.makedirs(archive_dir)

        output_dir = os.path.join(working_dir, "output")
        if os.path.isdir(output_dir) is False:
            os.makedirs(output_dir)

        tmp_dir = os.path.join(working_dir, "temp")
        if os.path.isdir(tmp_dir) is False:
            os.makedirs(tmp_dir)
        else:
            shutil.rmtree(tmp_dir)
            os.makedirs(tmp_dir)

        input_file_name = None
        input_file = None
        for input_file_name in os.listdir(input_dir):
            if input_file_name.endswith(".csv") is True:
                input_file = os.path.join(input_dir, input_file_name)
                gc.logwrite.info("")
                gc.logwrite.info("Lecture du fichier : " + input_file_name)

                input_file_name = input_file_name.replace(".csv", "")

                input_file_name_copy = os.path.join(output_dir, input_file_name + NEW_CSV)

                # Réécriture du fichier au bon format de lecture
                with open(input_file, 'r', encoding="cp1252") as open_file:
                    with open(input_file_name_copy, "w", encoding="cp1252") as my_file:
                        for line in open_file:
                            my_file.write(line)
                            my_file.flush()

                gc.logwrite.info("Création des listes de fichiers à envoyer à " + compagnie)

                colonnes = ["Cde COGEPRINT Client", "Adresse", "  N° Recommandé", "Statut", "Motif PND",
                            "Mise à jour", "Expédié le"]

                # Ecriture du fichier avec les exclusions hors compagnie
                open_file = pd.read_csv(input_file_name_copy, sep=";", encoding="cp1252")
                consolid = open_file.loc[open_file['Cde COGEPRINT Client'].str.startswith(inclusion), colonnes]

                consolid.to_csv(input_file_name_copy, sep=';')

                extract_file = input_file_name_copy
                if specif == "AXERIA":
                    extract_file = os.path.join(output_dir, input_file_name + "_picage.csv")

                    gc.logwrite.info("    Passage dans la specification AXERIA")
                    gc.logwrite.info("    Selection des documents en cours de plus de 20j")
                    # Selection des documents en cours de plus de 20j
                    extract1 = consolid.loc[open_file['Statut'] == 'En Cours', colonnes]

                    extract1[ET_MAJ] = pd.to_datetime(extract1[ET_MAJ], dayfirst=False, format=DATE_FORMAT)
                    extract1[ET_EXPEDIE] = pd.to_datetime(extract1[ET_EXPEDIE], dayfirst=False, format=DATE_FORMAT)

                    extract1[ET_DATE_DIFF] = pd.to_numeric((extract1[ET_MAJ] - extract1[ET_EXPEDIE]).dt.days)

                    extract1 = extract1.loc[(extract1[ET_DATE_DIFF] >= 20), :]

                    gc.logwrite.info("    Selection des documents en mode picage (NumReco = *****1/4)")
                    # Selection des documents en mode picage (NumReco = *****1/4)
                    extract2 = consolid.loc[(open_file[ET_NUM_RECO].str.endswith("1") |
                                             open_file[ET_NUM_RECO].str.endswith("4")), colonnes]
                    extract2[ET_MAJ] = pd.to_datetime(extract2[ET_MAJ], dayfirst=False, format=DATE_FORMAT)
                    extract2[ET_EXPEDIE] = pd.to_datetime(extract2[ET_EXPEDIE], dayfirst=False, format=DATE_FORMAT)
                    extract2[ET_DATE_DIFF] = 0

                    # Concaténation des DataFrames
                    liste_concat = [extract1, extract2]
                    extract3 = pd.concat(liste_concat)

                    gc.logwrite.info("    Suppression des doublons")
                    # Suppression des doublons de Numéro de reco
                    extract3 = extract3.drop_duplicates(subset=[ET_NUM_RECO])
                    # Ecriture du fichier d'extraction
                    gc.logwrite.info("    Ecriture du fichier : " + extract_file)
                    extract3.to_csv(extract_file, sep=';')

                    filefull = None
                    gc.logwrite.info("    Récupération des fichiers PDF retournés")
                    for files in os.listdir(output_dir):
                        if files.endswith("_picage.csv") is True:
                            extract_file = os.path.join(output_dir, files)
                        elif files.endswith(NEW_CSV) is True:
                            filefull = os.path.join(output_dir, files)

                    file_en_cours = extract_file.replace("_picage", "_en_cours")
                    file_pnd_distribue = extract_file.replace("_picage", "_picagenew")

                else:
                    gc.logwrite.info("    Ecriture du fichier : " + extract_file)
                    consolid.to_csv(extract_file, sep=';')
                    file_en_cours = extract_file.replace("_new", "_en_cours")

                # Archive du fichier d'entrée
                shutil.copy(input_file, archive_dir)

                # # Réécriture du fichier au bon format de lecture
                with open(extract_file, 'r', encoding="cp1252") as open_file:
                    with open(file_en_cours, "w", encoding="cp1252") as my_file:
                        with open(file_pnd_distribue, "w", encoding="cp1252") as my_file_picage:
                            for line in open_file:
                                reclamation = False
                                numcontrat = line.split(";")[1]
                                numreco = line.split(";")[3]
                                statut = line.split(";")[4]
                                if "Distri" in statut:
                                    statut = "DISTRIBUE"
                                elif "PND" in statut:
                                    statut = "PND"
                                elif "clamation" in statut:
                                    reclamation = True
                                else:
                                    statut = ""
                                sousrepertoire = numreco[0:9]
                                if reclamation is False:
                                    recherche = os.path.join(dirretourpdf, sousrepertoire,
                                                             numreco + "_" + numcontrat + "_" + statut + ".pdf")
                                    if os.path.isfile(recherche) is True:
                                        gc.logwrite.debug(RECUPERATION_DE + os.path.basename(recherche))
                                        if iszip is True:
                                            shutil.copy(recherche, tmp_dir)
                                    else:
                                        my_file.write(line)
                                        my_file.flush()

                                if reclamation is True:
                                    # On change les statuts si le fichier est en réalité arrivé.
                                    recherche = os.path.join(dirretourpdf, sousrepertoire,
                                                             numreco + "_" + numcontrat + "_" + "DISTRIBUE" + ".pdf")
                                    if os.path.isfile(recherche) is True:
                                        gc.logwrite.debug(RECUPERATION_DE + os.path.basename(recherche))
                                        if iszip is True:
                                            shutil.copy(recherche, tmp_dir)
                                        line = line.replace(line.split(";")[4], "Distribué")
                                    else:
                                        recherche = os.path.join(dirretourpdf, sousrepertoire,
                                                                 numreco + "_" + numcontrat + "_" + "PND" + ".pdf")
                                        if os.path.isfile(recherche) is True:
                                            gc.logwrite.debug(RECUPERATION_DE + os.path.basename(recherche))
                                            if iszip is True:
                                                shutil.copy(recherche, tmp_dir)
                                            line = line.replace(line.split(";")[4], "PND")
                                        else:
                                            my_file.write(line)
                                            my_file.flush()

                                my_file_picage.write(line)
                                my_file_picage.flush()

                # Remplacement du fichier à jour des statuts corrigés
                os.remove(extract_file)
                os.rename(file_pnd_distribue, extract_file)

                # Création du fichier zip contenant les retours LR
                if iszip is True:
                    gc.logwrite.info("    Création du fichier ZIP à retourner")
                    zipname = os.path.join(output_dir, input_file_name_copy + ".zip")
                    if os.path.isfile(zipname) is True:
                        os.remove(zipname)
                    zipfile = zipfile.ZipFile(zipname.replace(NEW_CSV, ""), "w")

                    with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
                        task = progress.add_task("Comp. " + compagnie, total=len(os.listdir(tmp_dir)))

                        for files in os.listdir(tmp_dir):
                            filePath = os.path.join(tmp_dir, files)
                            # Add file to zip
                            zipfile.write(filePath, os.path.basename(filePath))
                            # self.logwrite.debug("    Ajout du fichier : " + files)

                            progress.advance(task)

                        zipfile.write(extract_file, os.path.basename(extract_file))
                        zipfile.write(filefull, os.path.basename(filefull))

                    os.remove(extract_file)
                    os.remove(filefull)
                    zipfile.close()

                if specif != "AXERIA":
                    os.remove(file_en_cours)

        if input_file_name is None:
            gc.logwrite.warning("Pas de fichier à traiter")

        gc.logwrite.info("")
        gc.logwrite.info("Suppression des fichiers temporaires")

        shutil.rmtree(tmp_dir)

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
