import AOG.tools as tools
# 
import AOG.logfile as logfile
import AOG.sendmailtools as sendmailtools
import AOG.transferttools as transferttools
import AOG.logvolumetries as volumetries
import AOG.hcstools as hcstools

from datetime import datetime

from rich.progress import Progress, TimeElapsedColumn, SpinnerColumn
import os
import json
import shutil
import sys
import time
import zipfile

with open(os.path.join(os.path.dirname(__file__), 'appsettings.json')) as json_data:
    config = json.load(json_data)

if __name__ == "__main__":
    app_name = config[sys.argv[1]]["app_name"]
    model_name = config[sys.argv[1]]["model_name"]
    input_dir = config[sys.argv[1]]["input_dir"]
    transfert = config[sys.argv[1]]["transfert"]

    log = logfile.Logfile(name=app_name)

    ENV = config["ENV"]
    debug = config["debug"]
    _logmode = config["logInfo"]

    if _logmode in ["info", "warn", "debug", "error"]:
        logwrite = log.create_logger(_logmode, True)
    else:
        logwrite = log.create_logger("info", True)

    # Récupération des informatiosn de composition du modèles Designer
    paramtab = hcstools.lecture_tab(config["composition"]["op_wd"], model_name, logwrite)

    os.environ["opAppli"] = paramtab["opAppli"]
    os.environ["opFam"] = paramtab["opFam"]
    os.environ["op_wd"] = config["composition"]["op_wd"]
    PYTHONHOME = config["composition"]["PYTHONHOME"]
    os.environ["PYTHONHOME"] = PYTHONHOME
    opInstallDir = config["composition"]["opInstallDir"]
    os.environ["opInstallDir"] = opInstallDir
    os.environ["TMP"] = "D:/HCS/Platform/home/temp"
    os.environ["opEuro"] = "WINDOWS"
    os.environ["opDoubleByte"] = "1"
    os.environ["PYTHONPATH"] = opInstallDir + ";" + PYTHONHOME + "/DLLs;" + PYTHONHOME + "/lib;" + \
                               PYTHONHOME + "/lib/site-packages"
    os.environ["path"] = PYTHONHOME + ";" + opInstallDir + ";/bin;"
    os.environ["opTecMacroTab"] = "macros.tab," + \
                                  os.path.join(config["composition"]["hcsressourcespath"], "macros.tab")

    # On double les \ pour ne pas avoir de pb lors du techcodr
    workingdir = tools.get_working_directory(app_name).replace("\\", "\\\\")
    outdir = os.path.join(workingdir, "out")
    if os.path.isdir(outdir) is True:
        shutil.rmtree(outdir)
    os.makedirs(outdir)

    # Transfert data file
    data_dir_path = input_dir
    for files in os.listdir(data_dir_path):
        datafile = os.path.join(data_dir_path, files)
        if os.path.isfile(datafile):
            logwrite.info("Traitement du fichier " + datafile)
            shutil.copy(datafile, workingdir)

            # var de compo
            datafile = os.path.join(workingdir, files)
            vpffile = os.path.join(workingdir, "out_script.vpf")

            commandLine = config["composition"]["opInstallDir"] + os.sep + "bin" + os.sep + "pydlexec " + paramtab[
                          "dataloader"] + "_py -i " + datafile + " -o " + vpffile + " -V withVpfID=true" + \
                          " -V librairies=" + config["composition"]["hcsressourcespath"] + \
                          " -V cheminLogos=" + config["composition"]["cheminLogos"] + \
                          " -V cheminSignatures=" + config["composition"]["cheminSignatures"] + \
                          " -V cheminParams=" + config["composition"]["cheminParams"] + \
                          " -V STD5_TEMPLATE=" + paramtab["resdescid"] + ".xml" + \
                          " -V STD5_WITHDATA=YES -V STD5_OUTMODE=VPF " + \
                          " -V IDX_DATAFILE=" + os.path.basename(datafile) + \
                          " -E opMaxWarning=1 -E opFam=" + os.environ["opFam"] + \
                          " -E opAppli=" + os.environ["opAppli"] + " -E op_wd=" + os.environ["op_wd"] + \
                          " -E opDoubleByte=" + os.environ["opDoubleByte"] + \
                          " -E opTecMacroTab=" + os.environ["opTecMacroTab"] + \
                          " -E opInstallDir=" + config["composition"]["opInstallDir"]

            # Lancement Composition
            logwrite.info("Lancement de la composition pydlexec")
            logwrite.debug(commandLine)
            dateDebutStep = datetime.now()
            exit_code, pid, std_out, std_err = hcstools.run_command(commandLine)
            logwrite.info("Retour : " + str(exit_code))

            logwrite.info("Lancement du techsort")
            if exit_code != 0:
                # Probleme detecte
                erreur = exit_code
                logwrite.error("Erreur detectee a la composition : ExitCode[" + str(
                    exit_code) + "] / Erreur[" + str(std_out) + str(std_err) + "] / PID[" + str(pid) + "]")
                sys.exit(exit_code)

            logwrite.debug("Composition : ExitCode[" + str(exit_code) + "] / Retour[" + str(
                std_out) + "] / PID[" + str(pid) + "] / Duree[" + str(datetime.now() - dateDebutStep) + "]")

            commandLine = os.path.join(os.environ['opInstallDir'], 'bin', 'techsort') + \
                          ' -E opInstallDir=' + os.environ['opInstallDir'] + \
                          ' -E opMaxWarning=1' + \
                          ' -c ' + config["composition"]["hcsressourcespath"] + os.sep + 'dollar_doc.cmd' + \
                          ' -i ' + vpffile + \
                          ' -o ' + vpffile + "_s.vpf" + \
                          ' -S OUTPUTDIR=' + outdir + \
                          ' -S RESSOURCESDIR=' + config["composition"]["hcsressourcespath"] + \
                          ' -NamedIn -NamedOut' + \
                          ' -E opFam=' + os.environ["opFam"] + ' -E opAppli=' + os.environ["opAppli"] + ' -E op_wd=' + \
                          os.environ["op_wd"] + ' -E opDoubleByte=' + \
                          os.environ["opDoubleByte"]
            exit_code, pid, std_out, std_err = hcstools.run_command(commandLine)
            logwrite.debug(commandLine)
            logwrite.info("Retour : " + str(exit_code))

            if exit_code != 0:
                logwrite.error("Erreur detectee a l'enrichissement' : ExitCode[" + str(
                    exit_code) + "] / Erreur[" + str(std_out) + "/" + str(std_err) + "] / PID[" + str(pid) + "]")
                logwrite.error("Enrichissement : ExitCode[" + str(exit_code) + "] / Retour[" + str(
                    std_out) + "] / PID[" + str(pid) + "] / Duree[" + str(datetime.now() - dateDebutStep) + "]")
                sys.exit(exit_code)

            commandLine = os.path.join(os.environ['opInstallDir'], 'bin', 'techcodr') + \
                          " -E opInstallDir=" + os.environ['opInstallDir'] + \
                          " -DM " + config["composition"]["hcsressourcespath"] + \
                          " -iv " + vpffile + "_s.vpf" + \
                          " -dn pdf_ttf2" + \
                          " -od " + os.path.join(workingdir, "out_s.pdf")  # + \
            logwrite.info("Lancement du techcodr")
            dateDebutStep = datetime.now()
            exit_code, pid, std_out, std_err = hcstools.run_command(commandLine)
            logwrite.debug(commandLine)
            logwrite.info("Retour : " + str(exit_code))
            if exit_code != 0:
                # Probleme detecte
                logwrite.error("Erreur detectee a la mise en protocole : ExitCode[" + str(
                    exit_code) + "] / Erreur[" + str(std_out) + str(std_err) + "] / PID[" + str(pid) + "]")
                logwrite.error("Rendering : ExitCode[" + str(exit_code) + "] / Retour[" + str(
                    std_out) + "] / PID[" + str(pid) + "] / Duree[" + str(
                    datetime.now() - dateDebutStep) + "]")

            # Copie du fichier d'index dans l'usine à courrier
            indfile = vpffile + ".ind"
            indexfile = hcstools.IndFile(app_name, logwrite, compo_hcs=True)
            indexfile.get_hc_sindexfile(indfile)

            # Creation du zip de sortie
            nbdocs = 0
            ts = time.gmtime()
            ts = time.strftime("%Y%m%d", ts)
            listeplis = []
            zipname = os.path.join(workingdir, "ASSURONE_" + app_name.upper() + "_" + files + "_" + str(ts) + ".zip")
            zipoutput = zipfile.ZipFile(zipname, "w")
            logwrite.info("Ouverture du fichier zip : " + zipname)

            # Récupération des fichiers pdf à traiter
            for file in os.listdir(outdir):
                if file.endswith(".pdf") is True:
                    filePath = os.path.join(outdir, file)
                    logwrite.debug("      Ajout du fichier : " + file)
                    zipoutput.write(filePath, os.path.basename(filePath))
                    os.remove(filePath)
                    nbdocs = nbdocs + 1
            logwrite.info("Fermeture du fichier zip : " + zipname)

            # Upload des fichiers vers un prestataire
            if transfert is True:
                if ENV == "PROD":
                    upload = transferttools.upload_ftp(workingdir, logwrite, debug, nbdocs)
                    if ENV == "PROD":
                        logwrite.info("Début de l'étape de transfert FTP")
                        upload.uploadfiles()
                        logwrite.info("Fin de l'étape de transfert FTP")
                    else:
                        logwrite.warning("Environnement non PROD => Pas de transfert FTP")
                        upload.uploadfiles(False)
            else:
                logwrite.warning("Aucune transfert de fichier n'est prévu, il doit se faire manuellement")

            # Archivage du fichier de data
            archive_dir = os.path.join(input_dir, "archive")
            if os.path.isdir(archive_dir) is False:
                os.makedirs(archive_dir)
            shutil.copy(datafile, archive_dir)
            logwrite.info("Archivage du fichier de datas vers " + archive_dir)

            try:
                with open(indfile, "r") as f:
                    lines = f.readlines()
                    last_line = lines[-1]
                    print(len(lines))
                    nbplis = len(lines)
                    nbpages = int(last_line.split("\t")[0].split("_")[0]) + int(
                        last_line.split("\t")[0].split("_")[1]) + 1
                    prestataire = last_line.split("\t")[25]

                lv = volumetries.Volumetries("DIVERS", logwrite, year=dateDebutStep.year, env=ENV)

                ligne = 1
                while lv.ws.cell(ligne, 2).value is not None:
                    ligne = ligne + 1

                lv.ws.cell(ligne, 1).value = str(ts)[0:8]
                lv.ws.cell(ligne, 2).value = files
                lv.ws.cell(ligne, 3).value = app_name
                lv.ws.cell(ligne, 4).value = os.path.basename(zipname)
                lv.ws.cell(ligne, 5).value = prestataire
                lv.ws.cell(ligne, 6).value = nbplis
                lv.ws.cell(ligne, 7).value = nbpages

                logwrite.info("Ecriture des informations dans le fichier de volumétries")
                lv.closefile()
            except Exception as ex:
                print(str(ex))
                sys.exit(0)

            # Envoie du mail d'affranchissement
            mailrecap = sendmailtools.Sendmail()
            mailrecap.set_subject("Transfert d'un courrier one shot vers PREVOIR OK")
            body = "Le transfert des documents s'est déroulé sans erreur.\n\n"
            body = body + "   " + str(nbplis) + " plis ont été générés\n\n"
            body = body + "\n"
            body = body + "L'équipe éditique"
            mailrecap.set_body(body)
            mailrecap.add_attachment(log.get_log_path())
            mailrecap.set_low_priority()
            mailrecap.go_mail()
