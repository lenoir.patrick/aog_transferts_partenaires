#!python3
# -*- coding: utf-8 -*-

import AOG.logvolumetries as volumetries
import AOG.tools as aogtools
import AOG.sendmailtools as sendmailtools


class VerifVolumetriesConsommables:
    def __init__(self, typeconsommable, logwrite):
        self._last_value = None
        self._type_consommable = typeconsommable
        self._loaded_values = []
        self._last_value = 0
        self._previous_value = 0
        self._avg = 0
        self.logwrite = logwrite

    def load_values(self, values):
        for vol in values:
            # calcul des volumétries mensuelles
            if vol < self._last_value:
                diff = self._last_value - vol
                # print("    diff = " + str(diff))
                self._loaded_values.append(diff)
            self._previous_value = self._last_value
            self._last_value = vol

    def calc_average(self):
        try:
            self._avg = sum(self._loaded_values) / len(self._loaded_values)
        except (Exception,):
            pass

    def get_analyse(self):
        body = ""
        if self._avg == 0:
            msg = self._type_consommable
            body = body + msg + "\n"
            self.logwrite.error(msg)
            msg = "    Calcul impossible"
            body = body + msg + "\n\n"
            self.logwrite.error(msg)
        elif self._previous_value != self._last_value:
            if self._last_value > self._avg * 1.2:
                msg = self._type_consommable
                self.logwrite.info(msg)
                msg = "    Stock OK"
                self.logwrite.info(msg)
            else:
                msg = self._type_consommable
                body = body + msg + "\n"
                self.logwrite.warning(msg)
                msg = "    Volume restant " + str(self._last_value)
                body = body + msg + "\n"
                self.logwrite.warning(msg)
                msg = "    Moyenne mensuelle " + str(int(self._avg))
                body = body + msg + "\n"
                self.logwrite.warning(msg)
                msg = "    ==> Une commande est à envisager <=="
                body = body + msg + "\n\n\n"
                self.logwrite.warning(msg)
                # print(last_value)
        elif self._last_value == 0:
            msg = self._type_consommable
            self.logwrite.debug(msg)
            msg = "    Consommable plus utilisé"
            self.logwrite.debug(msg)
        return body


if __name__ == "__main__":
    appname = "REASSORT_CONSOMMABLES"
    gc = aogtools.GeneralConfig(appname)

    lv = volumetries.Volumetries("Consommables PREVOIR", gc.logwrite, generic=True)
    body = ""
    for i in range(3, 30):
        # On sort s'il n'y a plus d'entrée
        if lv.ws.cell(i, 2).value is None:
            break
            # L'entrée est trouvée, on renseigne la valeur

        type = lv.ws.cell(i, 2).value + " " + lv.ws.cell(i, 3).value

        vvc = VerifVolumetriesConsommables(type, gc.logwrite)

        cellidx = 4
        liste = []
        while lv.ws.cell(i, cellidx).value is not None:
            cellvalue = int(lv.ws.cell(i, cellidx).value)

            liste.append(cellvalue)
            cellidx = cellidx + 1

        vvc.load_values(liste)

        # calcul de la moyenne
        vvc.calc_average()

        # Interprétation du résultat
        body = body + vvc.get_analyse()

    mailrecap = sendmailtools.Sendmail()
    mailrecap.set_subject("Récap des commandes consommables PREVOIR à anticiper")
    if body == "":
        body = "Pas de commande à envisager"
        mailrecap.set_low_priority()
    else:
        mailrecap.set_high_priority()
    mailrecap.add_attachment(gc.log.get_log_path())
    mailrecap.set_body(body)
    mailrecap.go_mail()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
    