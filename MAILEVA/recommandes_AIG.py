# coding: utf-8
import os
import json

import AOG.logfile as logfile
import AOG.logvolumetries as volumetries
import AOG.pdftools as pdftools
import AOG.sendmailtools as sendmailtools
import AOG.tools as AOGtools
import AOG.hcstools as hcstools

with open(os.path.join(os.path.dirname(__file__), 'appsettings.json')) as json_data:
    config = json.load(json_data)

debug = config["debug"]
if debug is False:
    logInfo = config["logInfo"]
else:
    logInfo = "debug"

ENV = config['ENV']

repReco = config["recommandes"]["repReco"]
repRecoPrecontentieux = config["recommandes"]["repRecoPrecontentieux"]

class TransfertMaileva():
    def __init__(self):
        self.datetraitement = AOGtools.get_time_stamp_aaaammjj()

        self.appname = "maileva_lr"

        self.log = logfile.Logfile(name=self.appname)
        self._logmode = config['prod']

        if self._logmode in ["info", "warn", "debug", "error"]:
            self.logwrite = self.log.create_logger(self._logmode, True)
        else:
            self.logwrite = self.log.create_logger("info", True)

        self.sm = sendmailtools.Sendmail()

        # Initialisation de l'index commun HCS
        self.indexfile = hcstools.IndFile(self.appname, self.logwrite)
        self.indexfile.init_files()

    def liste_recommandes(self, moisrecup, repertoire):
        nbpagestot = 0
        for dirs in os.listdir(repertoire):
            if moisrecup[0:6] + "1" in dirs:
                daterecup = os.path.join(repertoire, dirs)
                print(daterecup)
                for files in os.listdir(daterecup):
                    files = os.path.join(daterecup, files)
                    nbpages = pdftools.getnbpagespdf(files)
                    nbpagestot = nbpagestot + nbpages
                    print(nbpages)
        return nbpagestot

#################################### DEBUT PROGRAMME ####################################
if __name__ == "__main__":
    tm = TransfertMaileva()
    nbreco = 0
    nbrecostd = tm.liste_recommandes(AOGtools.get_time_stamp_aaaammjj(), repReco)
    nbrecopc = tm.liste_recommandes(AOGtools.get_time_stamp_aaaammjj(), repRecoPrecontentieux)
    nbreco = nbrecostd + nbrecopc

lv = volumetries.Volumetries("DIVERS", logwrite, year=dateDebutStep.year, env=ENV)

