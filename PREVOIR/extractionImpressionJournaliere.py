# coding: utf-8
import datetime
import glob
import json
import os
import random
import re
import shutil
import sys
import uuid
import time
import zipfile
from datetime import date
from xml.dom.minidom import parseString
from xml.parsers.expat import ExpatError
from rich.progress import Progress, TimeElapsedColumn, SpinnerColumn

import AOG.logvolumetries as volumetries
import AOG.pdftools as pdftools
import AOG.sendmailtools as sendmailtools
import AOG.transferttools as transferttools
import AOG.hcstools as hcstools
import AOG.tools as aogtools
from AOG.tools import GeneralConfig
from AOG.usine_courrier.usine_courrier import get_produit_from_num_contrat
import AOG.constants as constants
import TF.api_request as TFRequest

with open(os.path.join(os.path.dirname(__file__), 'appsettings.json')) as json_data:
    config = json.load(json_data)

rep_pdf = config["dirs"]["rep_pdf"]
rep_adm = os.path.join(rep_pdf, config["dirs"]["rep_adm"])
rep_grdt = os.path.join(rep_adm, config["dirs"]["rep_grdt"])
repLotBlanc = os.path.join(rep_adm, config["dirs"]["rep_lot_Blanc"])
rep_flotte = os.path.join(rep_pdf, config["dirs"]["rep_flotte"])
rep_constructeurs = os.path.join(rep_pdf, config["dirs"]["rep_constructeurs"])

PDF_EXT = constants.PDF_EXT


class DocsToPrevoir(GeneralConfig):
    def __init__(self):
        # Initialisation
        self.appname = "PREVOIR_GESTION_STD"
        self.rep_travail = aogtools.get_working_directory(self.appname)
        super().__init__(self.appname)

        self.listedocs = []

        if self.is_env_prod():
            self.dirout = os.path.join(rep_grdt, config["dirs"]["outputdirPREVOIR"])
        else:
            self.dirout = config["dirs"]["tempdir"]

    def __del__(self):
        if os.path.isdir(self.rep_travail):
            shutil.rmtree(self.rep_travail)

    # retour date fichier
    @staticmethod
    def get_file_time_stamp(doc):
        return time.strftime("%Y%m%d", time.localtime(os.path.getmtime(doc)))

    def add_doc_to_list(self, doc_path, typedoc):
        try:
            doc = {}
            # 40762 - Traitement des fichiers vides
            filesize = (os.path.getsize(doc_path))
            if filesize > 10240:
                doc['path'] = doc_path
                doc['type'] = typedoc
                doc['name'] = os.path.basename(doc_path)
                doc['pages'] = pdftools.getnbpagespdf(doc_path)
                doc['contrat'] = self.get_ref_contrat(doc_path)
                doc['papier_email'] = "PAPIER"
                for item in config["dontprint"]:
                    if item in doc['name']:
                        doc['papier_email'] = "EMAIL"
                self.logwrite.debug("ajout de " + str(doc))
                self.listedocs.append(doc)
            else:
                self.logwrite.warning("Le fichier : " + str(os.path.basename(doc_path)) + " | fait moins de 10 Ko")
        except Exception as ex:
            self.logwrite.warning("Erreur de lecture pour le fichier : " + str(os.path.basename(doc_path)) +
                                  " | " + str(ex))

    # @param
    #       date String Date d'un jour egal ou anterieur a aujourd'hui
    # @Traitement
    #       Liste l'ensemble des documents à imprimer du jour passe en argument
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def list_doc_jour(self, date):
        self.logwrite.info("Debut liste doc du jour : " + date)
        liste_doc_flotte = []
        liste_doc_grdt = []
        liste_doc_constructeurs = []
        liste_doc_lot_blanc = []
        liste_doc_flotte = dtp.list_doc_jour_flotte(date)
        liste_doc_grdt = dtp.list_doc_jour_grdt(date)
        liste_doc_constructeurs = dtp.list_doc_jour_constructeur(date)
        liste_doc_lot_blanc = dtp.list_doc_jour_lot_blanc(date)
        list_doc_jour = liste_doc_flotte + liste_doc_grdt + liste_doc_constructeurs + liste_doc_lot_blanc
        return list_doc_jour

    # @param
    #       date String Date format d'un jour egal ou anterieur a aujourd'hui, dit le jour sans fin
    # @Traitement
    #       Liste l'ensemble des documents à imprimer pour la Flotte du jour passe en argument
    #       On parcourt les documents en sélectionnant ceux du jour dans les sous rep suivants :
    #           - cv : fichiers commençant par "CVDEFINITIVE"
    #           - contrats : fichiers commençant par "CP-FLOTTE-"
    #       N.B : les docs sont nommées de la sorte :
    #           - cv : CVDEFINITIVE-FLOTTE-04F101661-01-1-20210928-092548.PDF
    #           - contrats : CP-FLOTTE-04F101735-01.PDF ; CP-FLOTTE-04F100955-02-20210929-095531-DUPLICATA.PDF
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def list_doc_jour_flotte(self, date):
        self.logwrite.info("Debut liste doc du jour de la flotte : " + date)
        listdoc_flotte = []

        rep_flotte_cv = rep_flotte + "\\cv"
        rep_flotte_cp = rep_flotte + "\\contrats"

        # PARTIE VERIF CV
        self.logwrite.info("repFlotteCV = " + rep_flotte_cv)
        type_doc = "FlotteCV"
        # 19/08/22 : evolution avec mise en place de la date dans la recherche pour ameliorer les temps de performances
        # 19/08/22 : en analyse, pas de cas où le fichier est mal date
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(glob.glob(rep_flotte_cv + '\\*-' + date + '-*')))
            for doc_cv in glob.glob(rep_flotte_cv + '\\*-' + date + '-*'):
                progress.advance(task)
                # on ne garantit jamais que l'entité est un fichier, qui sait qu'un excentrique a mis un sous repertoire
                if os.path.isfile(doc_cv):
                    if self.get_file_time_stamp(doc_cv) == date:
                        pattern = re.compile("CVFACS.*")
                        # Si ce n'est pas une facsimile
                        # /!\ ATTENTION - cette partie peut poser problème si
                        # les facsimiles deviennent des attestations --- A VOIR
                        if not pattern.match(os.path.basename(doc_cv)):
                            listdoc_flotte.append(doc_cv)
                            self.add_doc_to_list(doc_cv, type_doc)

        self.logwrite.info("")
        # PARTIE VERIF CP
        type_doc = "FlotteCP"
        self.logwrite.info("repFlotteCP = " + rep_flotte_cp)
        # on ne prend que les cp - d'apres mes verifications le nom de tous les pdf de CP commencent par "CP"
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(glob.glob(rep_flotte_cp + '\\CP*')))
            for doc_cp in glob.glob(rep_flotte_cp + '\\CP*'):
                progress.advance(task)

                # comme d'hab par mesure de securite on vérifie que ça soit bien un fichier
                if os.path.isfile(doc_cp):
                    # verification de la date
                    if self.get_file_time_stamp(doc_cp) == date:
                        self.add_doc_to_list(doc_cp, type_doc)
                        listdoc_flotte.append(doc_cp)

        self.logwrite.info("")
        return listdoc_flotte

    # @param
    #       date String Format AAAAMMJJ Date d'un jour egal ou anterieur a aujourd'hui
    # @Traitement
    #       Liste l'ensemble des documents à imprimer pour GRDT du jour passe en argument
    #       Dans le REP GRDT (Constante "rep_grdt" )
    #           parcourir l'ensemble des sous repertoires (AUTO, AUTO_TEMPO, CCAR, CYCLO, MOTO, VSP)
    #           dans chaque sous repertoires, aller dans le repertoire de la date du jour " type AAAAMMJJ"
    #           Lister l'ensemble des docs des sous repertoires presentS dans le rep de la date du jour
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def list_doc_jour_grdt(self, date):
        self.logwrite.info("Debut liste doc du jour des branches GRDT : " + date)
        list_doc_grdt = []
        type_doc = "GRDT"
        self.logwrite.info("rep_grdt = " + rep_grdt)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(glob.glob(rep_grdt + '\\*')))
            for rep_branche in glob.glob(rep_grdt + '\\*'):
                progress.advance(task)
                # on vérifie que c'est bien un repertoire
                # exemple \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO
                if os.path.isdir(rep_branche):
                    rep_branche_date = os.path.join(rep_branche, date)
                    # on vérifie l'existance de ce repertoire date
                    # exemple :  \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO\20210929
                    if os.path.isdir(rep_branche_date):
                        # on boucle sur chacune des ressources du repertoire date
                        for rep_branche_date_rep_type_doc in glob.glob(rep_branche_date + '\\*'):
                            # on vérifie si la ressource est un repertoire et donc un repertoire de document
                            # exemple bon :  \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO\20210929\CP
                            # exemple faux : \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO\20210929\CV_AUTO.pdf
                            if os.path.isdir(rep_branche_date):
                                # on parcourt le sous repertoire de document en listant les docs pdf
                                for doc in glob.glob(rep_branche_date_rep_type_doc + '\\*'):
                                    # on s'assure que ça soit quand meme un fichier, par acquis de conscience
                                    if os.path.isfile(doc):
                                        # et on ajoute le doc dans la liste
                                        list_doc_grdt.append(doc)
                                        self.add_doc_to_list(doc, type_doc)

        self.logwrite.info("")
        return list_doc_grdt

    # @param
    #       date String Date d'un jour egal ou anterieur a aujourd'hui
    # @Traitement
    #       Liste l'ensemble des documents à imprimer pour les Constructeurs du jour passe en argument
    #       Dans le sous rep "rep_constructeurs" selection le rep date AAAAMMJJ
    #       Dans le rep date / AUTO, prendre l'ensemble des documents des sous repertoires
    #       (exemple : ECHEANCIER, devis, cv, CP, AVENANT )
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def list_doc_jour_constructeur(self, date):
        self.logwrite.info("Debut liste doc du jour des Constructeurs : " + date)
        list_doc_constructeurs = []
        rep_construct_date = os.path.join(rep_constructeurs, date, "AUTO")
        # Dans ce repertoire il y a differents sous repertoire par type de doc
        # il y a aussi des pdfs qui sont realisee apres fusion.
        # On doit parcourir uniquement les sous repertoires pour avoir les pdf unitaires
        type_doc = "Constructeurs"
        self.logwrite.info("repConstructDate = " + rep_construct_date)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(glob.glob(rep_construct_date + '\\*')))
            for rep_type_doc in glob.glob(rep_construct_date + '\\*'):
                progress.advance(task)
                if os.path.isdir(rep_type_doc):
                    # Si c'est bien un repertoire on le parcourt et on liste l'ensemble des fichiers contenus
                    for doc in glob.glob(rep_type_doc + '\\*'):
                        if os.path.isfile(doc):
                            list_doc_constructeurs.append(doc)
                            self.add_doc_to_list(doc, type_doc)
        self.logwrite.info("")
        return list_doc_constructeurs

    # @param
    #       AUCUN
    # @Traitement
    #       On va chercher dans le répertoire de prod où l'ensemble des documents pdf est depose avant fusion
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def list_doc_jour_lot_blanc(self, date):
        self.logwrite.info("Debut liste doc du jour des lots blancs (hors GRDT/FLOTTE/CONSTRUCTEURS) : " + date)
        list_doc_blanc = []

        # self.logwrite.info(repLotBlanc)
        type_doc = "BLANC"
        self.logwrite.info("repLotBlanc = " + repLotBlanc)
        """datecompare = str(date[0:4]) + "-" + str(date[4:6]) + "-" + str(date[6:8])
        for dirs in os.listdir(repLotBlanc):
            if datecompare == dirs:
                # print(dirs)
                dir = os.path.join(repLotBlanc, dirs)
                self.logwrite.info("repLotBlanc = " + dir)
                # for doc in glob.glob(repLotBlanc+'\\*'):
                for docs in os.listdir(dir):"""
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(os.listdir(repLotBlanc)))
            for docs in os.listdir(repLotBlanc):
                progress.advance(task)
                # on vérifie que c'est bien un repertoire
                # exemple \\10.128.3.120\cortex\prod\pdf\ADM\DEMANDE_IMPRESSION\prevoir
                doc = os.path.join(repLotBlanc, docs)
                if os.path.isfile(doc):
                    # 19/08/22 : Maintenance à chaud = usine... à faire evoluer pour rendre plus propre
                    if os.path.basename(doc) != 'Thumbs.db':
                        # et on ajoute le doc dans la liste
                        list_doc_blanc.append(doc)
                        self.add_doc_to_list(doc, type_doc)

        self.logwrite.info("")
        return list_doc_blanc

    # @param
    #       chemin_fichier_impression String chemin absolu du pdf à imprimer
    # @Traitement
    #       Recuperation du nom du fichier à partir du chemin absolu
    #       Si le CP et un DUPLICATA sont envoyés, on ne gardera que le dernier fichier car le tableau est trié alpha
    # @Return
    #    String nom du document
    @staticmethod
    def get_nom_doc(chemin_fichier_impression):
        filename = os.path.split(chemin_fichier_impression)[-1]
        # filename = filename.replace("-DUPLICATA", "")
        return filename

    # @param
    #       nom_fichier_impression String chemin / nom du pdf à imprimer
    # @Traitement
    #       Tous les pdf, a l'heure actuelle sont nommes de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String la reference contrat
    @staticmethod
    def get_ref_contrat(nom_fichier_impression):
        pattern = re.compile("^([^-]+-[^-]+)-([^-]+-[^-]+)(-[^-]+)*\.[pdf|PDF]")
        # On dirait des smileys
        nom_fichier = os.path.basename(nom_fichier_impression)
        matcher = pattern.match(nom_fichier)
        return matcher.group(2)

    # @param
    #       ref_contrat String type num_contrat - ref piece
    # @Traitement
    #       Récupération du numéro de contrat
    #       Tous les pdf, a l'heure actuelle sont nommes de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String le numero du contrat
    @staticmethod
    def get_num_contrat(ref_contrat):
        regex_pattern = '^([^-]+)-([^-]+)'
        if re.match(regex_pattern, ref_contrat) is not None:
            pattern = re.compile(regex_pattern)
            matcher = pattern.match(ref_contrat)
        return matcher.group(1)

    # @Return
    # string le numero de contrat
    # dans le cas de la branche flotte, on peut avoir un contrat mais plusieurs immatricules, on recupere le compteur
    @staticmethod
    def getcounter(nom_fichier_impression):
        regex_pattern = re.compile("^([^-]+-[^-]+)-([^-]+-[^-]+)-((100)|(0*\d{1,2}))(-[^-]+)*\.[pdf|PDF]")
        if re.match(regex_pattern, nom_fichier_impression) is not None:
            pattern = re.compile(regex_pattern)
            matcher = pattern.match(nom_fichier_impression)
            return matcher.group(3)
        else:
            return None

    # @param
    #       ref_contrat String type num_contrat - ref piece
    # @Traitement
    #       Idealement il faudrait une verification en base pour verifier
    #       qu'il ne s'agisse pas d'une enieme demande de generation mais
    #       on ne peut pas affirmer que tout soit trace en base car c'est un fait
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #       Boolean    True si ref piece == "01"
    @staticmethod
    def is_affaire_nouvelle(ref_contrat):
        regex_pattern = '^([^-]+)-([^-]+)'
        if re.match(regex_pattern, ref_contrat) is not None:
            pattern = re.compile(regex_pattern)
            matcher = pattern.match(ref_contrat)
            if matcher.group(2) == "01":
                return True
        return False

    '''def isduplicata(self, nom_fichier_impression):
        regexPattern = re.compile("(^[^-]).*-(DUPLICATA)\.[pdf|PDF]")
        if re.match(regexPattern, nom_fichier_impression) is not None:
            pattern = re.compile(regexPattern)
            matcher = pattern.match(nom_fichier_impression)
            return matcher.group(2)
        else:
            return ""'''

    # @param
    #       nom_fichier_impression String nom du pdf à imprimer
    # @Traitement
    #       Tous les pdf, a l'heure actuelle sont nommés de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION : il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String la Branche
    @staticmethod
    def get_branche(nom_fichier_impression):
        pattern = re.compile("^([^-]+)-([^-]+)-([^-]+-[^-]+)(-[^-]+)*\.[pdf|PDF]")
        # On dirait des smileys
        nom_fichier = os.path.basename(nom_fichier_impression)
        matcher = pattern.match(nom_fichier)
        return matcher.group(2)

    # @param
    #       nom_fichier_impression String nom du pdf a imprimer
    # @Traitement
    #       Tous les pdf, a l'heure actuelle sont nommes de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String Type du document
    @staticmethod
    def get_type_doc(nom_fichier_impression):
        pattern = re.compile("^([^-]+)-([^-]+)-([^-]+-[^-]+)(-[^-]+)*\.[pdf|PDF]")
        # On dirait des smileys
        nom_fichier = os.path.basename(nom_fichier_impression)
        matcher = pattern.match(nom_fichier)
        return matcher.group(1)

    # @param
    #       liste_doc_unique    String[] Liste de document a transferer
    # @Traitement
    #       Transfert les documents à imprimer dans le repertoir prevoir
    # @Return
    #       String[] Liste de liens absolus des documents dans un tableau / array
    def transfert_liste_doc(self, liste_doc_unique, rep_cible):
        list_docs_transferes = []
        list_docs_non_transferes = []
        self.logwrite.info("Debut transfert de fichier vers " + rep_cible)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(liste_doc_unique))
            for doc_unique in list(reversed(liste_doc_unique)):
                # for docUnique in liste_doc_unique:
                progress.advance(task)
                # Cas d'exclusion des documents à ne pas imprimer
                # dontprint = False
                # for item in config["dontprint"]:
                #    if item in docUnique:
                #         dontprint = True
                # docUnique['impression_papier'] = dontprint
                # print(docUnique)
                for item in config["dontprint"]:
                    if item in doc_unique:
                        list_docs_non_transferes.append(doc_unique)

                try:
                    if isinstance(doc_unique, str):
                        chemin_fichier_transfere = shutil.copy(doc_unique,
                                                               os.path.join(rep_cible, self.get_nom_doc(doc_unique)))
                    else:
                        chemin_fichier_transfere = shutil.copy(doc_unique['path'],
                                                               os.path.join(rep_cible,
                                                                            self.get_nom_doc(doc_unique['name'])))
                    list_docs_transferes.append(chemin_fichier_transfere)
                except shutil.SameFileError as error:
                    self.logwrite.warning(
                        "Erreur : c'est le même chemin pour le fichier entre la source et la destination.")
                    self.logwrite.warning(error)
                    list_docs_non_transferes.append(chemin_fichier_transfere)
                except PermissionError as error:
                    self.logwrite.warning("Erreur : Permission denied.")
                    self.logwrite.warning(error)
                    list_docs_non_transferes.append(chemin_fichier_transfere)
                except Exception as ex:
                    self.logwrite.warning("Error occurred while copying file.")
                    self.logwrite.warning(str(ex))
                    list_docs_non_transferes.append(chemin_fichier_transfere)
        self.logwrite.info("Fin transfert de fichier vers " + rep_cible)
        return list_docs_transferes, list_docs_non_transferes

    # @param
    #       num_contrat    String[] numero du contrat
    # @Traitement
    #       Extraction des donnees 'marque apporteur' et 'numéro de courtier' du flux XML retourne par l'API
    # @Return
    #       Dict{}    Dictionnaire contenant deux couples clé:valeur avec les cles marqueApporteur et numCourtier
    def get_donnees_mise_sous_pli(self, num_contrat, type_doc):
        marque_apporteur = ""
        num_courtier = ""
        reference_contrat = ""

        # Recuperation des donnees via l'API
        donnees_xml = TFRequest.get_response_xml(num_contrat, type_doc, self.logwrite)
        # On teste si on a un xml vide ou non
        if donnees_xml is not None and donnees_xml != "":
            # Creation du parseur et validation du flux XML
            parser = parseString(donnees_xml)
            # On instancie le Document DOM
            data_documents = parser.getElementsByTagName('DataDocumentViewModel')
            for data_document in data_documents:
                # On vérifie si le la balise 'NomApporteur' contient l'attribut 'i:nil' qui signifie qu'elle est vide
                # if dataDocument.getElementsByTagName('NomApporteur')[0].hasAttribute('i:nil') == True:
                #    marqueApporteur = 'VIDE'
                #    numCourtier = dataDocument.getElementsByTagName('CodeCourtier')[0].firstChild.data
                #    donneesMiseSousPli = {'marqueApporteur': marqueApporteur, 'numCourtier': numCourtier}
                # else:
                #    marqueApporteur = dataDocument.getElementsByTagName('NomApporteur')[0].firstChild.data
                reference_contrat = num_contrat
                num_courtier = data_document.getElementsByTagName('CodeCourtier')[0].firstChild.data

                # On vérifie si le la balise 'numCourtier' contient l'attribut 'i:nil' qui signifie qu'elle est vide
                if not data_document.getElementsByTagName('CodeCourtier')[0].hasAttribute('i:nil'):
                    marque_apporteur = num_courtier[:4]
                if not data_document.getElementsByTagName('ReferenceContrat')[0].hasAttribute('i:nil'):
                    reference_contrat = data_document.getElementsByTagName('ReferenceContrat')[0].firstChild.data

        else:
            if type_doc == "SIN":
                reference_contrat = num_contrat + "-00"
            else:
                reference_contrat = num_contrat
            marque_apporteur = "VIDE"
            num_courtier = "VIDE"

        donnees_mise_sous_pli = {'marqueApporteur': marque_apporteur,
                                 'numCourtier': num_courtier,
                                 'ReferenceContrat': reference_contrat}
        return donnees_mise_sous_pli

    # @param
    #       liste_doc_unique_arenommer    String[] Liste des documents à imprimer tranferes dans le repertoire Prevoir
    # @Traitement
    #       Renommage de tous les fichiers PDF comme suit SAUF DEVIS :
    #       [TYPE_DOC]-[BRANCHE]-[N°_CONTRAT]-[N°_PIECE]-[MARQUE_APPORTEUR]-[N°_COURTIER]-[DATE]
    #       Si on est le cas type document = DEVIS :
    #       on garde la nomenclature : [DEVIS]-[BRANCHE]-[NUMERO_DEVIS]-[MARQUE_APPORTEUR]-[N°_COURTIER]-[DATE]
    # @Return
    #       String[] Liste de liens absolus des documents dans un tableau / array
    def renommage_fichiers_impression(self):
        list_fichiers_renommes = []
        list_nom_cvprovisoire = ["CVERTE_PRO"]
        list_nom_cvdefinitive = ["CV", "CVD", "CV_REMORQUE_AXA", "CV_REMORQUE_EQUITE", "CARTES_VERTES", "CARTE_VERTE"]
        # duplicata = ""
        # for chemin_fichier_impression in liste_doc_unique_arenommer:
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(self.listedocs))
            for fichier in list(reversed(self.listedocs)):
                progress.advance(task)

                chemin_fichier_impression = fichier["path"]
                nom_fichier_impression = self.get_nom_doc(chemin_fichier_impression)
                nom_fichier_impression_info = nom_fichier_impression.split("-")
                # duplicata = self.isduplicata(chemin_fichier_impression)

                # CAS CHIEN-CHAT pour DEVIS/CONTRAT : La branche CHIEN-CHAT avec le "-" comme split provoque une
                # separation non souhaitée par rapport aux autres cas On va donc conditionner pour avoir une liste
                # conforme par rapport aux autres cas
                if nom_fichier_impression_info[1] == "CHIEN" and nom_fichier_impression_info[2] == "CHAT":
                    # on renomme, pour le nom de la branche, le 2e element : nomFichierImpressionInfo[1] = "CHIENCHAT"
                    nom_fichier_impression_info[1] = nom_fichier_impression_info[1] + nom_fichier_impression_info[2]
                    # on corrige donc la liste en supprimant le 3e element : nomFichierImpressionInfo[2] = "CHAT" pour
                    # avoir l'element suivant i.e le num CONTRAT/DEVIS/SIN
                    del nom_fichier_impression_info[2]
                    nom_fichier_impression = nom_fichier_impression.replace("CHIEN-CHAT", "CHIENCHAT")

                # CAS DUPLICATA, on rajoute la mention
                '''if duplicata != "":
                    duplicata = "-" + duplicata
                    chemin_fichier_impression = chemin_fichier_impression.replace("-DUPLICATA", "")'''

                donnees_api = {"marqueApporteur": "VIDE", "numCourtier": "VIDE", "ReferenceContrat": "VIDE"}

                # CAS DEVIS
                if nom_fichier_impression_info[0] == "DEVIS":
                    type_doc = "DEVIS"
                    branche = nom_fichier_impression_info[1]
                    num_devis = nom_fichier_impression_info[2]
                    # le numero DEVIS ne fonctionne pas actuellement avec editique-API et ne l'utilise pas
                    # marqueApporteur = donneesAPI["marqueApporteur"]
                    # numCourtier = donneesAPI["numCourtier"]
                    fichier_nouveau_nom = type_doc + "-" + branche + "-" + num_devis + "-VIDE-VIDE-" + date

                # CAS Documents SINISTRE/INDEMNISATION (type doc commencant par SIN)
                elif nom_fichier_impression_info[0].startswith("SIN"):
                    type_doc = nom_fichier_impression_info[0]
                    branche = nom_fichier_impression_info[1]
                    num_sin = nom_fichier_impression_info[2]
                    count = "0"
                    # le numero sinistre fonctionne depuis 07/22 avec editique-API
                    try:
                        donnees_api = self.get_donnees_mise_sous_pli(num_sin, "SIN")
                    except ExpatError as error:
                        self.logwrite.warning("Erreur sur le fichier : " + chemin_fichier_impression)
                        self.logwrite.warning(str(error))
                    except Exception as ex:
                        self.logwrite.warning("Unexpected error : " + str(ex))

                    marque_apporteur = donnees_api["marqueApporteur"]
                    num_courtier = donnees_api["numCourtier"]
                    ref_contrat = donnees_api["ReferenceContrat"]

                    fichier_nouveau_nom = "{0}-{1}-{2}-{3}-{4}-{5}-{6}".format(type_doc, branche, ref_contrat,
                                                                               marque_apporteur, num_courtier, date,
                                                                               count)

                # CAS GENERAL
                else:
                    type_doc = self.get_type_doc(nom_fichier_impression)
                    branche = self.get_branche(nom_fichier_impression)
                    ref_contrat = self.get_ref_contrat(nom_fichier_impression)
                    num_contrat = self.get_num_contrat(ref_contrat)
                    try:
                        donnees_api = self.get_donnees_mise_sous_pli(num_contrat, "")
                    except ExpatError as error:
                        self.logwrite.warning("Erreur sur le fichier : " + chemin_fichier_impression)
                        self.logwrite.warning(str(error))
                    except Exception as error:
                        self.logwrite.warning("Unexpected error : " + str(error))

                    marque_apporteur = donnees_api["marqueApporteur"]
                    num_courtier = donnees_api["numCourtier"]
                    count = "0"
                    if branche == "FLOTTE":
                        if type_doc == "CVPROV":
                            type_doc = "ATTPROV"
                        if self.getcounter(nom_fichier_impression) is not None:
                            count = "-" + self.getcounter(nom_fichier_impression)

                    if type_doc in list_nom_cvdefinitive:
                        type_doc = "CVDEFINITIVE"
                    elif type_doc in list_nom_cvprovisoire:
                        type_doc = "CVPROV"

                    fichier_nouveau_nom = "{0}-{1}-{2}-{3}-{4}-{5}-{6}".format(type_doc, branche, ref_contrat,
                                                                               marque_apporteur, num_courtier, date,
                                                                               count)

                chemin_nouveau_fichier_impression = os.path.join(self.rep_travail, fichier_nouveau_nom + PDF_EXT)

                self.logwrite.debug("   Old name : " + chemin_fichier_impression)
                self.logwrite.debug("   New name : " + chemin_nouveau_fichier_impression)
                try:
                    # self.logwrite.info(os.path.join(self.rep_travail, os.path.basename(chemin_fichier_impression)))
                    os.rename(os.path.join(self.rep_travail, os.path.basename(chemin_fichier_impression)),
                              chemin_nouveau_fichier_impression)
                    list_fichiers_renommes.append(chemin_nouveau_fichier_impression)
                except (FileExistsError, ):
                    # exemple:
                    # CVPROV-AUTO-04M37970-01-20220406-120757
                    # CVPROV-AUTO-04M37970-01-20220406-122107
                    # Ce sont des numeros de contrat identique mais l' 'heure de creation est differente
                    # Hors dans le renommage, on omet l' 'heure de creation
                    # Des tests effectues, on constate que ce sont des pdfs avec des contenus identiques
                    self.logwrite.info(
                        "suppression du doublon : " + chemin_fichier_impression)
                    os.remove(chemin_fichier_impression)
                except (FileNotFoundError, ):
                    # On recherche si le fichier n'a pas deja ete renomme
                    if chemin_nouveau_fichier_impression in list_fichiers_renommes:
                        self.logwrite.info("Le fichier existe deja " + chemin_nouveau_fichier_impression)
                        continue

        return list_fichiers_renommes

    def calcul_volumetries(self, liste_doc_unique_arenommer, date_trt):
        self.logwrite.info("Début de la mise à jour du fichier de volumétries")
        try:
            year = int(date_trt[0:4])
            month = int(date_trt[4:6])
            day = int(date_trt[6:8])

            voldocs = {}
            volconsommables = {}

            # Initialisation de l'index commun HCS
            indexfile = hcstools.IndFile(self.appname, self.logwrite)
            indexfile.init_files()

            num_doc = 0
            nb_feuilles = 1

            infos = []
            nb_pages_doc = 1
            # Récupération des volumes pour chaque type de documents
            for file in liste_doc_unique_arenommer:
                if file != "Thumbs.db":
                    items = os.path.basename(file).split("-")
                    typedoc = items[0]
                    branche = items[1]
                    numcontrat = items[2]
                    numpiece = items[3]
                    marque = items[4]

                    self.logwrite.debug(file)
                    self.logwrite.debug("    " + str(items))
                    nb_pages_doc = pdftools.getnbpagespdf(file)

                    infos = indexfile.init_infos()
                    infos["NomFichierEntree"] = os.path.basename(file)
                    infos["NomFichierSortie"] = os.path.basename(file)

                    infos["ReferenceContrat"] = numcontrat
                    infos["Branche"] = branche
                    infos["Produit"] = get_produit_from_num_contrat(numcontrat)
                    print(infos["Produit"])
                    infos["Marque"] = items[4]
                    infos["Courtier"] = items[5]
                    infos["TypeDocCortex"] = typedoc

                    infos["canalDistribution"] = "IMPRESSION"
                    infos["refDistribution"] = "PREVOIR"

                    for item in config["dontprint"]:
                        if item in os.path.basename(file):
                            infos["canalDistribution"] = "EMAIL"
                            infos["refDistribution"] = "ASSURONE"
                            break

                    infos["nbPages"] = nb_pages_doc
                    infos["nbFeuilles"] = int((nb_pages_doc + 1) / 2)

                    # Ajout usine courrier et le suivi des plis
                    infos["uuidpli"] = str(uuid.uuid4())
                    # print(infos["uuidpli"])
                    if infos["canalDistribution"] == "IMPRESSION":
                        pdftools.add_datamatrix(file, infos["uuidpli"])

                    if typedoc in voldocs.keys():
                        voldocs[typedoc]['nb_docs'] = voldocs[typedoc]['nb_docs'] + 1
                        voldocs[typedoc]['nbPages'] = voldocs[typedoc]['nbPages'] + nb_pages_doc
                    else:
                        item = {'nb_docs': 1, 'nbPages': nb_pages_doc}
                        voldocs[typedoc] = item

                    if marque[0:2] == "NE":
                        marque = "NETVOX"
                    elif marque not in ["PEUG", "CITR", "DSIN", "MERC", "SMAR", "SKOD", "SEAT", "MAZD", "VOLK", "LBPH"]:
                        marque = "NEUTRE"

                    # Calcul des consommables
                    if typedoc in ['CVDEFINITIVE', 'CVPROV', 'ENVOI_PV_CONSTAT'] and branche in ['MOTO', 'CYCLO']:
                        consommable = "PV2R"
                        infos["refPV"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)
                    elif typedoc == "CP" and numpiece == "01"\
                            and branche in ['AUTO', 'CCAR', 'VSP'] and marque != "GRDT":
                        consommable = "PV4R-" + marque
                        infos["refPV"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)
                    elif typedoc == "ENVOI_PV_CONSTAT" and branche in ['AUTO', 'CCAR', 'FLOTTE']:
                        consommable = "PV4R-" + marque
                        infos["refPV"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)
                    elif typedoc == "CVDEFINITIVE" and branche == 'FLOTTE':
                        consommable = "PV4R-" + marque
                        infos["refPV"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)
                    elif typedoc == "CVDEFINITIVE" and branche in ['AUTO', 'VSP'] and marque == "GRDT":
                        consommable = "PV4R-AOL"
                        infos["refPV"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)

                    if typedoc == "CP" and numpiece == "01" \
                            and branche in ['AUTO', 'CYCLO', 'MOTO', 'CCAR', 'FLOTTE', 'VSP']:
                        # print("Constat Amiable Auto")
                        consommable = "CONSTAT-AUTO"
                        infos["refConstat"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)

                    if typedoc == "CP" and numpiece == "01" and branche == "MRH":
                        # print("Constat Amiable Degat des Eaux")
                        consommable = "CONSTAT-EAU"
                        infos["refConstat"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)
                    elif typedoc == "ENVOI_PV_CONSTAT" and branche == "MRH":
                        # print("Constat Amiable Degat des Eaux")
                        consommable = "CONSTAT-EAU"
                        infos["refConstat"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)

                    # Enveloppes
                    if nb_pages_doc > 8:
                        consommable = "ENV-C4"
                        infos["TypeEnveloppe"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)
                    else:
                        consommable = "ENV-C5"
                        infos["TypeEnveloppe"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)

                    # Pages Blanches / Carte Verte
                    if typedoc in ['CVDEFINITIVE', 'CVPROV']:
                        consommable = "PAPIER-CV"
                        infos["TypePapier"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, 1)
                    else:
                        consommable = "PAPIER-BLANC"
                        infos["TypePapier"] = consommable
                        volconsommables = self.set_volumetrie_consommable(volconsommables, consommable, nb_pages_doc)

                # Ecriture de l'index HCS à la page
                indexfile.writelinesdoc(nb_pages_doc, num_doc, infos)

                # Incrementation des compteurs
                nb_feuilles = nb_feuilles + nb_pages_doc
                num_doc = num_doc + 1

            # indexfile.close()

            d0 = datetime.date(year, 1, 1)
            d1 = datetime.date(year, month, day)
            delta = d1 - d0

            # Ecriture dans le fichier Excel de suivi
            lv = volumetries.Volumetries("PREVOIR_PAGES", self.logwrite, year=year, env=self.env)
            self.logwrite.info(
                "Quantième du jour : " + str(delta.days) + " - Décalage de  : " + str(delta.days) + " colonne(s)")

            for item in voldocs:
                for i in range(3, 250):
                    # L'entrée est trouvée, on renseigne la valeur
                    if item == lv.ws.cell(i, 2).value:
                        # Décalage du quantième du jour et placement de la bonne valeur
                        lv.ws.cell(i, 3 + delta.days).value = voldocs[item]['nbPages']
                        self.logwrite.debug(
                            item + " - " + str(i) + ":" + str(3 + delta.days - 1) + " - " + str(
                                voldocs[item]['nbPages']))
                        break

                    # L'entrée n'existe pas, on la crée
                    elif lv.ws.cell(i, 2).value is None:
                        lv.ws.cell(i, 2).value = item
                        lv.ws.cell(i, 3 + delta.days).value = voldocs[item]['nbPages']
                        self.logwrite.info(
                            item + "-" + str(i) + ":" + str(3 + delta.days - 1) + " - " + str(voldocs[item]['nbPages']))
                        break
            lv.closefile()

            lv = volumetries.Volumetries("PREVOIR_PLIS", self.logwrite, year=year, env=self.env)
            for item in voldocs:
                for i in range(3, 250):
                    # L'entrée est trouvée, on renseigne la valeur
                    if item == lv.ws.cell(i, 2).value:
                        # Décalage du quantième du jour et placement de la bonne valeur
                        lv.ws.cell(i, 3 + delta.days).value = voldocs[item]['nb_docs']
                        self.logwrite.debug(
                            item + " - " + str(i) + ":" + str(3 + month) + " - " + str(voldocs[item]['nb_docs']))
                        break

                    # L'entrée n'existe pas, on la crée
                    elif lv.ws.cell(i, 2).value is None:
                        lv.ws.cell(i, 2).value = item
                        lv.ws.cell(i, 3 + delta.days).value = voldocs[item]['nb_docs']
                        self.logwrite.debug(
                            item + "-" + str(i) + ":" + str(3 + delta.days - 1) + " - " + str(voldocs[item]['nb_docs']))
                        break
            lv.closefile()

            """lv = volumetries.Volumetries("CONSOMMABLES", self.logwrite, year=year, env=self.env)
            for item in volconsommables:
                print(item)
                itemName = item
                if "PV4R" in itemName:
                    try:
                        if "NE" in itemName:
                            itemName = "PV4R-NETVOX"
                        elif itemName[9:13] in ["FORD", "OPEL", "VOLV", "BMWA", "MINI", "VIDE"]:
                            itemName = "PV4R-NEUTRE"
                    except:
                        pass
                print(itemName)

                for i in range(3, 20):
                    # L'entrée est trouvée, on renseigne la valeur
                    if item == lv.ws.cell(i, 2).value:
                        # Décalage du quantième du jour et placement de la bonne valeur
                        val = lv.ws.cell(i, 3 + month - 1).value
                        if val is None:
                            newval = volconsommables[item]['unites']
                        else:
                            newval = int(lv.ws.cell(i, 3 + month - 1).value) + int(volconsommables[item]['unites'])
                        lv.ws.cell(i, 3 + month - 1).value = newval
                        self.logwrite.debug(
                            item + " - " + str(i) + ":" + str(3 + month - 1) + " - " + str(newval))
                        break

                    # L'entrée n'existe pas, on la crée
                    elif lv.ws.cell(i, 2).value is None:
                        lv.ws.cell(i, 2).value = item
                        lv.ws.cell(i, 3 + month - 1).value = volconsommables[item]['unites']
                        self.logwrite.debug(
                            item + "-" + str(i) + ":" + str(3 + month - 1) + " - " + str(
                                volconsommables[item]['unites']))
                        break
            lv.closefile()"""
            return voldocs, volconsommables
        except Exception as ex:
            print(str(ex))
            sys.exit(0)

    @staticmethod
    def set_volumetrie_consommable(volconsommables, consommable, nb_pages):
        if consommable in volconsommables.keys():
            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + nb_pages
        else:
            volconsommables[consommable] = {'unites': nb_pages}
        return volconsommables

    def purge(self, rep):
        self.logwrite.info("Purge de " + rep)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task(constants.PROGRESSION, total=len(glob.glob(rep + '\\*')))
            for files in glob.glob(rep + '\\*'):
                progress.advance(task)
                if os.path.isfile(files):
                    os.remove(files)
                    self.logwrite.debug(files)
        self.logwrite.info("Purge terminée")

    @staticmethod
    def _is_exclusion(docname, liste):
        for i in range(0, 2):
            if docname.split("-")[i] in liste:
                return True
        return False

    def compression(self, rep_du_jour_lot_blanc, date_du_jour):
        nb_docs = 0
        nb_docs_err = 0
        lsite_doc_err = []

        listexclusion = config["validationimpression"]
        dtp.logwrite.info("    Validation des impressions pour : " + str(listexclusion))

        zipname_prevoir = os.path.join(rep_du_jour_lot_blanc, "ASSURONE_" + date_du_jour + ".zip")
        zip_prevoir = zipfile.ZipFile(zipname_prevoir, "w")
        self.logwrite.info("  Ouverture du fichier zip : " + zipname_prevoir)

        zipname_err = os.path.join(rep_du_jour_lot_blanc, "ERR_" + date_du_jour + ".zip")
        zip_err = zipfile.ZipFile(zipname_err, "w")
        self.logwrite.info("  Ouverture du fichier zip d'exclusion : " + zipname_err)

        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task("    Compression ", total=(len(os.listdir(rep_du_jour_lot_blanc))))
            for file in os.listdir(rep_du_jour_lot_blanc):
                progress.advance(task)
                file_path = os.path.join(rep_du_jour_lot_blanc, file)
                if file.endswith(PDF_EXT) is True:
                    if self._is_exclusion(file, listexclusion) is False:
                        self.logwrite.debug("      Ajout du fichier : " + file)
                        zip_prevoir.write(file_path, os.path.basename(file_path))
                        os.remove(file_path)
                        nb_docs = nb_docs + 1
                    else:
                        self.logwrite.warning("      Ajout du fichier exclus : " + file)
                        zip_err.write(file_path, os.path.basename(file_path))
                        lsite_doc_err.append(file)
                        os.remove(file_path)
                        nb_docs_err = nb_docs_err + 1

        self.logwrite.info("  Fermeture du fichier zip : " + zipname_prevoir)
        self.logwrite.info("  Fermeture du fichier zip : " + zipname_err)
        if nb_docs_err > 0:
            self.logwrite.info("  il y a " + str(nb_docs_err) + " docs en erreur")
            self.logwrite.info("  envoie d'un mail de recap")
            mail_doc_error = sendmailtools.Sendmail()
            mail_doc_error.set_subject("[PREVOIR] Documents non imprimés ce jour")
            body = "Liste des docs en erreur sur la production du jour.\n\n"
            body = body + "Voir le fichier : " + zipname_err + "\n"
            for file in lsite_doc_err:
                body = body + "    - " + file + "\n"
            mail_doc_error.set_body(body)
            mail_doc_error.set_high_priority()
            # mailDocError.add_attachment(zipnameERR)
            mail_doc_error.go_mail()
        return nb_docs


def creation_repertoire_date_du_jour(rep):
    if not os.path.exists(rep):
        os.makedirs(rep)
    return rep


if __name__ == "__main__":
    # J-1 format %Y-%m-%d
    dateprod = datetime.date.today()
    # J-1 Format %Y%m%d
    # for i in reversed(range(2, 104)):
    # for i in (range(203, 241)):
    # date = (dateprod + datetime.timedelta(days=i - 241)).strftime("%Y%m%d")
    date = (dateprod + datetime.timedelta(days=-1)).strftime("%Y%m%d")

    # J Format %Y-%m-%d
    # dateprod = "2023-08-01"
    date_du_jour = str(dateprod)

    # DEBUT PROGRAMME
    dtp = DocsToPrevoir()
    mailrecap = sendmailtools.Sendmail()
    try:
        dtp.logwrite.info("Traitement du " + str(dateprod) + " pour les fichiers du {date}".format(date=date))
        dtp.logwrite.info("=== DEBUT PROGRAMME ==")
        dtp.logwrite.info("")

        # On liste tous les docs des branches : GRDT(AOL)/Flotte/AUTO
        dtp.logwrite.info("=== DEBUT LISTE docs des branches : GRDT(AOL)/Flotte/AUTO ==")
        dtp.list_doc_jour(date)
        dtp.logwrite.info("=== FIN LISTE docs des branches : GRDT(AOL)/Flotte/AUTO ==")
        dtp.logwrite.info("")

        # On transfert le lot vers le repertoire de travail pour renommage des fichiers
        dtp.logwrite.info("=== DEBUT TRANSFERT REP TRAVAIL ==")
        liste_doc_unique_arenommer = dtp.transfert_liste_doc(dtp.listedocs, dtp.rep_travail + "\\")
        dtp.logwrite.info("=== FIN TRANSFERT REP TRAVAIL ==")
        dtp.logwrite.info("")
        
        # On renomme les documents avec l'appel de l'API editique pour les standardiser
        dtp.logwrite.info("=== DEBUT RENOMMAGE NOMS VIA API ==")
        liste_doc_unique_impression = dtp.renommage_fichiers_impression()
        dtp.logwrite.info("=== FIN RENOMMAGE NOMS VIA API ==")
        dtp.logwrite.info("")

        voldocs, volconsommables = dtp.calcul_volumetries(liste_doc_unique_impression, date)

        date_du_jour_dir = date_du_jour.split("-")
        # On créé le repertoire date du jour dans le repertoire prevoir du lot blanc et 
        # on range les PDFs du jour dans le nouveau repertoire cree
        dtp.logwrite.info("=== DEBUT creation repertoire et transfert du lot blanc du jour ==")
        dtp.logwrite.info("Creation du répertoire {0}".format(
            os.path.join(dtp.dirout, date_du_jour_dir[0].zfill(2), date_du_jour_dir[1].zfill(2),
                         date_du_jour_dir[2].zfill(2))))
        rep_du_jour_lot_blanc = creation_repertoire_date_du_jour(os.path.join(dtp.dirout, date_du_jour_dir[0].zfill(2), 
                                                                              date_du_jour_dir[1].zfill(2), 
                                                                              date_du_jour_dir[2].zfill(2)))

        liste_transfere, liste_non_transfere = dtp.transfert_liste_doc(liste_doc_unique_impression, 
                                                                       rep_du_jour_lot_blanc)

        dtp.logwrite.info("=== FIN creation repertoire du jour et transfert du lot blanc du jour ==")

        # Chargement des exclusions (documents KO à imprimer)
        dtp.logwrite.info("Chargement des documents à envoyer dans un zip")
        nb_docs = dtp.compression(rep_du_jour_lot_blanc, date_du_jour)
        dtp.logwrite.info("Fin de Chargement des documents à envoyer dans un zip")

        # Upload des fichiers vers Prevoir
        if dtp.is_env_prod():
            dtp.logwrite.info("Début de l'étape de transfert FTP")
            upload = transferttools.UploadFtp(rep_du_jour_lot_blanc, logwrite=dtp.logwrite, debug=dtp.is_debug_mode(),
                                              pathcmd=os.path.dirname(__file__))
            upload.setfiles(nb_docs, fileformat=".zip", prefix="ASSURONE")
            upload.uploadfiles()
            dtp.logwrite.info("Fin de l'étape de transfert FTP")
        else:
            dtp.logwrite.warning("Environnement non PROD => Pas de transfert FTP")
            upload = transferttools.UploadFtp(rep_du_jour_lot_blanc, logwrite=dtp.logwrite, debug=dtp.is_debug_mode(),
                                              pathcmd=os.path.dirname(__file__))
            upload.setfiles(nb_docs, fileformat=".zip", prefix="ASSURONE")
            upload.uploadfiles(False)

        # Envoie du mail de récap
        mailrecap.set_subject("Transfert des documents vers Prévoir OK")
        body = "Le transfert des documents vers Prevoir s'est déroulé sans erreur.\n\n"
        body = body + "Les informations de volumétries ont été mises à jour.\n"

        for item in sorted(voldocs):
            body = body + "   " + str(item) + " : " + str(voldocs[item]) + "\n"

        body = body + "\nLes consommables utilisés sont les suivants.\n"
        for item in sorted(volconsommables):
            body = body + "   " + str(item) + " : " + str(volconsommables[item]['unites']) + "\n"

        if len(liste_non_transfere) > 0:
            body = body + "\nDes éléments n'ont pas été transférés :\n"
            for item in liste_non_transfere:
                body = body + "   " + item + "\n"
        mailrecap.set_low_priority()

        if dtp.is_env_prod():
            mailrecap.add_attachment(os.path.join(rep_du_jour_lot_blanc, "sftp.txt"))

        # Picage aléatoire pour validation des documents
        dtp.logwrite.info("Picages aléatoires pour vérification des documents")
        nbDocPicage = 0
        listdocs = os.listdir(dtp.rep_travail)
        nbdocs = len(listdocs)

        try:
            if config["picage"]["active"] is True:
                mailpicage = sendmailtools.Sendmail()
                mailpicage.set_subject("Liste des picages aléatoires du jour")
                mailpicage.set_high_priority()
                nbpicages = 0
                for i in range(0, config["picage"]["nombre"]):
                    trouve = False
                    nbTry = 0
                    while trouve is False:
                        nbTry = nbTry + 1
                        it = random.randint(0, nbdocs - 1)
                        typedoc = listdocs[it].split("-")[0]
                        if typedoc not in config["picage"]["liste"]:
                            trouve = True
                            mailpicage.add_attachment(os.path.join(dtp.rep_travail, listdocs[it]))
                            nbpicages = nbpicages + 1
                            dtp.logwrite.info("     Sélection du document : " + listdocs[it])

                        # porte de sortie de la boucle
                        if nbTry == 30:
                            break

                if nbpicages > 0:
                    mailpicage.go_mail()
                dtp.logwrite.info("Fin de l'étape de picages")
            else:
                dtp.logwrite.info("La feature est désactivée")
        except Exception as ex:
            dtp.logwrite.error("Erreur lors du picage : " + str(ex))

        # Récupération de documents spéciaux

        if config["picage"]["active"] is True:
            fichiers = []
            for file in os.listdir(dtp.rep_travail):
                if file.endswith(PDF_EXT):
                    fichiers.append(os.path.join(dtp.rep_travail, file))
            pdftools.findspecificwordinpdf(fichiers, "Gratien", dtp.logwrite)

            pdftools.findspecificwordinpdf(fichiers, "Signature_", dtp.logwrite)

        if dtp.is_debug_mode() is False:
            dtp.logwrite.info("=== DEBUT Purge du jour du rep lot blanc  ==")
            dtp.purge(repLotBlanc)
            # dtp.purge(dtp.rep_travail)
            shutil.rmtree(dtp.rep_travail)
            dtp.logwrite.info("=== FIN Purge du jour du rep lot blanc  ==")

    except Exception as ex:
        dtp.logwrite.error(str(ex))
        mailrecap.set_subject("Transfert des documents vers Prévoir KO")
        mailrecap.set_high_priority()
        body = "Une erreur est survenue lors du traitement des documents vers prévoir.\n"
        body = body + "Ci-joint la log d'éxécution pour analyse et retraitement.\n"
    except KeyboardInterrupt:
        if dtp.is_debug_mode():
            shutil.rmtree(dtp.rep_travail)
        sys.exit(0)

    body = body + "\n"
    body = body + "L'équipe éditique"
    mailrecap.set_body(body)
    mailrecap.add_attachment(dtp.log.get_log_path())

    try:
        mailrecap.go_mail()
    except (Exception, ):
        dtp.logwrite.info(body)

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(dtp.appname)
