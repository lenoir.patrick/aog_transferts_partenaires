import AOG.tools as tools
# 
import AOG.logfile as logfile
import AOG.sendmailtools as sendmailtools
import AOG.transferttools as transferttools
import AOG.logvolumetries as volumetries
import AOG.hcstools as hcstools

from datetime import datetime

from rich.progress import Progress, TimeElapsedColumn, SpinnerColumn
import os
import json
import shutil
import sys


#TODO variabilier la valeur des lignes pour le fichier de volumétries

with open(os.path.join(os.path.dirname(__file__), '../appsettings.json')) as json_data:
    config = json.load(json_data)


if __name__ == "__main__":
    appname = "courrier_oneshot"
    ENV = "DEV"
    debug = True

    log = logfile.Logfile(name=appname)
    _logmode = "info"

    if _logmode in ["info", "warn", "debug", "error"]:
        logwrite = log.create_logger(_logmode, True)
    else:
        logwrite = log.create_logger("info", True)

    paramtab = hcstools.lecture_tab(config["composition"]["op_wd"], "Courrier_Allianz_202212", logwrite)

    os.environ["opAppli"] = paramtab["opAppli"]
    os.environ["opFam"] = paramtab["opFam"]

    os.environ["op_wd"] = config["composition"]["op_wd"]
    PYTHONHOME = config["composition"]["PYTHONHOME"]
    os.environ["PYTHONHOME"] = PYTHONHOME
    opInstallDir = config["composition"]["opInstallDir"]
    os.environ["opInstallDir"] = opInstallDir
    os.environ["TMP"] = "D:/HCS/Platform/home/temp"
    os.environ["opEuro"] = "WINDOWS"
    os.environ["opDoubleByte"] = "1"
    os.environ["PYTHONPATH"] = opInstallDir + ";" + PYTHONHOME + \
                               "/DLLs;" + PYTHONHOME + "/lib;" + PYTHONHOME + "/lib/site-packages"
    os.environ["path"] = PYTHONHOME + ";" + opInstallDir + ";/bin;"
    os.environ["opTecMacroTab"] = "macros.tab," + os.path.join(config["composition"]["hcsressourcespath"],
                                                               "macros.tab")

    # On double les \ pour ne pas avoir de pb lors du techcodr
    workingdir = tools.get_working_directory(appname).replace("\\", "\\\\")
    outdir = os.path.join(workingdir, "out")
    if os.path.isdir(outdir) is True:
        shutil.rmtree(outdir)
    os.makedirs(outdir)

    # Transfert data file
    datafile = os.path.join("D:\\scripts\\transfert\\PREVOIR", "courrier_v2.csv")
    shutil.copy(datafile, workingdir)

    # var de compo
    datafile = os.path.join(workingdir, "courrier_v2.csv")
    vpffile = os.path.join(workingdir, "out_script.vpf")

    commandLine = config["composition"]["opInstallDir"] + os.sep + "bin" + os.sep + "pydlexec " + paramtab["dataloader"] + "_py" \
        " -i " + datafile + \
        " -o " + vpffile + \
        " -V withVpfID=true" + \
        " -V librairies=" + config["composition"]["hcsressourcespath"] + \
        " -V cheminLogos=" + config["composition"]["cheminLogos"] + \
        " -V cheminSignatures=" + config["composition"]["cheminSignatures"] + \
        " -V cheminParams=" + config["composition"]["cheminParams"] + \
        " -V STD5_TEMPLATE=" + paramtab["resdescid"] + ".xml" \
        " -V STD5_WITHDATA=YES -V STD5_OUTMODE=VPF " + \
        " -V IDX_DATAFILE=" + os.path.basename(datafile) + \
        " -E opMaxWarning=1 -E opFam=" + os.environ["opFam"] + " -E opAppli=" + os.environ["opAppli"] + " -E op_wd=" + \
        os.environ["op_wd"] + " -E opDoubleByte=" + os.environ["opDoubleByte"] + \
        " -E opTecMacroTab=" + os.environ["opTecMacroTab"] + \
        " -E opInstallDir=" + config["composition"]["opInstallDir"]

    # Lancement Composition
    logwrite.info("Lancement de la composition pydlexec")
    logwrite.debug(commandLine)
    dateDebutStep = datetime.now()
    exitCode, pid, stdOut, stdErr = hcstools.run_command(commandLine)
    logwrite.info("Retour : " + str(exitCode))

    logwrite.info("Lancement du techsort")
    if (exitCode != 0):
        # Probleme detecte
        erreur = exitCode
        logwrite.error("Erreur detectee a la composition : ExitCode[" + str(
            exitCode) + "] / Erreur[" + str(stdOut) + str(stdErr) + "] / PID[" + str(pid) + "]")
        sys.exit(exitCode)

    logwrite.debug("Composition : ExitCode[" + str(exitCode) + "] / Retour[" + str(
        stdOut) + "] / PID[" + str(pid) + "] / Duree[" + str(datetime.now() - dateDebutStep) + "]")


    commandLine = os.path.join(os.environ['opInstallDir'], 'bin', 'techsort') + \
                  ' -E opInstallDir=' + os.environ['opInstallDir'] + \
                  ' -E opMaxWarning=1' + \
                  ' -c ' + config["composition"]["hcsressourcespath"] + os.sep + 'dollar_doc.cmd' + \
                  ' -i ' + vpffile + \
                  ' -o ' + vpffile+ "_s.vpf" + \
                  ' -S OUTPUTDIR=' + outdir + \
                  ' -S RESSOURCESDIR=' + config["composition"]["hcsressourcespath"] + \
                  ' -NamedIn -NamedOut' + \
                  ' -E opFam=' + os.environ["opFam"] + ' -E opAppli=' + os.environ["opAppli"] + ' -E op_wd=' + \
                  os.environ["op_wd"] + ' -E opDoubleByte=' + \
                  os.environ["opDoubleByte"]
    exitCode, pid, stdOut, stdErr = hcstools.run_command(commandLine)
    logwrite.debug(commandLine)
    logwrite.info("Retour : " + str(exitCode))

    if exitCode != 0:
        logwrite.error("Erreur detectee a l'enrichissement' : ExitCode[" + str(
            exitCode) + "] / Erreur[" + str(stdOut) + "/" + str(stdErr) + "] / PID[" + str(pid) + "]")
        logwrite.error("Enrichissement : ExitCode[" + str(exitCode) + "] / Retour[" + str(
            stdOut) + "] / PID[" + str(pid) + "] / Duree[" + str(datetime.now() - dateDebutStep) + "]")
        sys.exit(exitCode)

    # Copie du fichier d'index dans l'usine à courrier
    indexfile = hcstools.IndFile(appname, logwrite, compo_hcs=True)
    indexfile.get_hc_sindexfile(vpffile + ".ind")

    commandLine = os.path.join(os.environ['opInstallDir'], 'bin', 'techcodr') + \
          " -E opInstallDir=" + os.environ['opInstallDir'] + \
          " -DM " + config["composition"]["hcsressourcespath"] + \
          " -iv " + vpffile + "_s.vpf" + \
          " -dn pdf_ttf2" + \
          " -od " + os.path.join(workingdir, "out_s.pdf") # + \

    dateDebutStep = datetime.now()
    # break
    logwrite.debug(commandLine)
    exitCode, pid, stdOut, stdErr = hcstools.run_command(commandLine)
    if (exitCode != 0):
        # Probleme detecte
        logwrite.error("Erreur detectee a la mise en protocole : ExitCode[" + str(
            exitCode) + "] / Erreur[" + str(stdOut) + str(stdErr) + "] / PID[" + str(pid) + "]")
        logwrite.error("Rendering : ExitCode[" + str(exitCode) + "] / Retour[" + str(
            stdOut) + "] / PID[" + str(pid) + "] / Duree[" + str(
            datetime.now() - dateDebutStep) + "]")

    try:
        lv = volumetries.Volumetries("ONESHOT", logwrite, year=datetrt_annee, env=ENV)
        lv.ws.cell(int(ligne), int(datetrt_mois) + 2).value = nbpages
        logwrite.info(str(ligne) + ":" + str(int(datetrt_mois) + 2) + " - " + str(nbpages))
        lv.closefile()
    except (Exception, ):
        pass

    # Upload des fichiers vers Prevoir
    if ENV == "PROD":
        logwrite.info("Début de l'étape de transfert FTP")
        upload = transferttools.UploadFtp(workingdir, logwrite=logwrite, debug=debug,
                                          pathcmd=os.path.dirname(__file__))
        upload.setfiles(nbdocs, fileformat=".zip", prefix="ASSURONE")
        nbdocs = upload.zipfiles(workingdir, outdir, logwrite)
        upload.uploadfiles()
        logwrite.info("Fin de l'étape de transfert FTP")
    else:
        logwrite.warning("Environnement non PROD => Pas de transfert FTP")
        upload = transferttools.UploadFtp(workingdir, logwrite=logwrite, debug=debug,
                                          pathcmd=os.path.dirname(__file__))
        nbdocs = upload.zipfiles(workingdir, outdir, logwrite)
        upload.setfiles(nbdocs, fileformat=".zip", prefix="ASSURONE")
        upload.uploadfiles(False)

    # Envoie du mail d'affranchissement
    mailrecap = sendmailtools.Sendmail()
    mailrecap.set_subject("Transfert d'un courrier one shot vers PREVOIR OK")
    body = "Le transfert des documents vers PREVOIR s'est déroulé sans erreur.\n\n"
    body = body + "   " + str(nbdocs) + " plis ont été transmis\n\n"
    body = body + "\n"
    body = body + "L'équipe éditique"
    mailrecap.set_body(body)
    mailrecap.add_attachment(log.get_log_path())
    mailrecap.set_low_priority()
    mailrecap.go_mail()