``` json
{
    "pnd": {
        "mailpnd": "plenoir@assurone-group.com",
        "mailpnvyv": "plenoir@assurone-group.com;lenoir.patrick@gmail.com",
        "dirpnd": "\\\\A1-srv-p-sftp01\\RECEPTION\\TERMES\\PND"
    },
    "recommandes": {
        "repVerifServiceClient": "\\\\10.128.3.120\\cortex\\depots\\poste",
        "active_verif": true,
        "repReco": "\\\\10.128.3.120\\cortex\\depots\\poste\\courriers_recommandes",
        "repRecoRejet": "\\\\10.128.3.120\\cortex\\depots\\poste\\courriers_recommandes_rejets",
        "repRecoPrecontentieux": "\\\\10.128.3.120\\cortex\\depots\\poste\\courriers_recommandes\\precontentieux",
        "repCogeprint": "C:\\\\Users\\plenoir\\Documents\\DOF_Travail\\LR_COGEPRINT",
        "mail_qualite_gestion": "plenoir@assurone-group.com",
        "dirbdf": "\\\\A1-srv-p-sftp01\\NOTIFICATION",
        "dirretourpdf": "\\\\A1-srv-p-sftp01\\NOTIFICATION\\RETOURS_LR",
        "retour_recommandes":{
            "clients": [
                {
                    "nom": "AXERIA", 
                    "filtreregex": "AXE",
                    "email": "",
                    "active": false
                },
                {
                    "nom": "VYV", 
                    "filtreregex": "CPMP|CPSO|CPVY|CPMG|CCHA|CCMG",
                    "email": "plenoir@assurone-group.com;lenoir.patrick@gmail.com",
                    "active": true
                }
            ]
        }
    },
    "lettres_cheques": {
        "repLC": "\\\\A1-srv-p-sftp01\\RECEPTION\\LC\\",
        "repPND": "\\\\A1-srv-p-sftp01\\RECEPTION\\PND\\"
    },
    "debug": false,
    "prod": "info",
    "logInfo": "info",
    "ENV": "DEV"
}
```

| **champ**   | **description**                                     |
|-------------|-----------------------------------------------------|
| **debug**   | debug activé (true/false)                           |
| **prod**    | ??                                                  |
| **loginfo** | exhaustivité des logs (error, warning, info, debug) |
| **env**     | type d'environnement (DEV, PPROD, PROD)             |


# pnd
| **champ**     | **description**                            |
|---------------|--------------------------------------------|
| **mailpnd**   | Mail d'envoie des listings PND Termes      |
| **mailpnvyv** | Mail d'envoie des listings PND LR pour VYV |
| **dirpnd**    | Répertoire où trouver les PND TERMES       |

# recommandes
| **champ**                 | **description**                                              |
|---------------------------|--------------------------------------------------------------|
| **repVerifServiceClient** | Répertoire où se trouve les fichiers de vérification         |
| **active_verif**          | Activer la vérification des envoies LR (true/false           |
| **repReco**               | Repertoire des recommandés                                   |
| **repRecoRejet**          | Répertoire des recommandés rejet                             |
| **repRecoPrecontentieux** | Répertoire des recommandés précontentieux                    |
| **repCogeprint**          | Répertoire de dépôt des fichiers                             |
| **mail_qualite_gestion**  | Email pour le service qualité gestion                        |
| **dirbdf**                | Répertoire de traitement des bons de fabrication             |
| **dirretourpdf**          | Répertoire où trouver les fichiers d'affranchissement des LR |

## retour_recommandes / clients
> Section récursive qui permet des traitements spécifiques par client/partenaire sur la gestion des courriers recommandés en PND

| **champ**               | **description**                                       |
|-------------------------|-------------------------------------------------------|
| **nom**                 | nom du spécifique client                              |
| **filtreregex**         | filtre permettant de retrouver les courriers relatifs |
| **email (non utilisé)** | email pour envoie du recap                            |
| **active**              | specif actif (true/false)                             |

# lettres_cheques
| champ  | description                                |
|--------|--------------------------------------------|
| repLC  | Répertoire où trouver le fichier à traiter |
| repPND | Répertoire où trouver les fichiers PND     |



