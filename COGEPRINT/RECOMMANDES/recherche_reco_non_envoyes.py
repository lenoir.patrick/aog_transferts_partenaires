#!python3
# -*- coding: utf-8 -*-

import json
import re
import os
import shutil
import AOG.tools as aogtools
import AOG.constants as constants
import AOG.sendmailtools as sendmailtools

with open(os.path.join(os.path.dirname(__file__), '../appsettings.json')) as json_data:
    config = json.load(json_data)

repVerifServiceClient = config["recommandes"]["repVerifServiceClient"]
repReco = config["recommandes"]["repReco"]
repRecoRejet = config["recommandes"]["repRecoRejet"]
repRecoPrecontentieux = config["recommandes"]["repRecoPrecontentieux"]


class RechercheReco:
    def __init__(self, expr_rgx, logwrite=None):
        self.expr_rgx = expr_rgx
        self.datafile = None
        self.nbpdf = None
        self.nbpdftrouve = None
        self.nbtrouvesousrep = None
        self.nbfichieratrouver = None
        self.liste_date_full = {}

        self.datetraitement = aogtools.get_time_stamp_aaaammjj()

        if logwrite is None:
            self.appname = "COGEPRINT_LR_COMPARE"
            gc = aogtools.GeneralConfig(self.appname)
            self.logwrite = gc.logwrite
        else:
            self.logwrite = logwrite

    def get_dates_traitement(self):
        for files in os.listdir(repVerifServiceClient):
            completefile = os.path.join(repVerifServiceClient, files)
            if os.path.isfile(completefile) is True and files.endswith(".csv"):
                for name in ["Precontentieux_gestion_", "Precontentieux_primes_", "Contrats_resilies_"]:
                    if name in files:
                        datejourfichier = files.split("_")[2].split(".")[0]
                        datejourfichier = datejourfichier[0:4] + "-" + datejourfichier[4:6] + "-" + datejourfichier[6:8]
                        self.liste_date_full[datejourfichier] = ""
        if len(self.liste_date_full) > 0:
            self.logwrite.info("Liste des journées à traiter pour la vérification : " + str(self.liste_date_full))
        else:
            self.logwrite.warning("Aucun fichier de vérification à traiter")

    def load(self, file, datejour):
        '''
        file = fichier à charger
        datejour = date du traitement
        '''
        self.nbpdf = 0
        self.nbpdftrouve = 0
        self.datafile = file
        self.logwrite.info("Chargement du fichier : " + file)
        listelr = []
        self.liste_reco = {}

        if not os.path.isfile(file):
            self.logwrite.warning("Le fichier " + file + " n'est pas présent")
        else:
            with open(file, 'r', encoding="cp1252") as openfile:
                for line in openfile:
                    numcontrat = line.split(";")[1].split("-")[0].strip()
                    demandelr = line.split(";")[4][0:16]

                    # Chargement des entrées dans le fichier .csv
                    if numcontrat != 'ref_contrat':
                        if demandelr != "Pas de LR demand":
                            listelr.append(numcontrat)
                            self.nbpdf = self.nbpdf + 1
                            self.liste_reco[datejour] = listelr
                        else:
                            self.logwrite.debug("    Le contrat " + numcontrat + " n'a pas de LR demandée")

            self.logwrite.debug("Chargement du fichier terminé")

    def run(self, rootpath, liste_lr_transferees):
        """ Lance le traitement principal sur le répertoire sélectionné

        Args:
            rootpath: répertoire de travail par défaut
            liste_lr_transferees: liste des lr à rechercher

        """
        # Parcours différentes journées
        for item in self.liste_date_full:
            datelr = item

            # La journée existe dans les fichiers chargés
            rep = os.path.join(rootpath, datelr)
            if os.path.isdir(rep) is True:
                self.logwrite.info("    Parcours du répertoire " + str(rep))

                self.nbtrouvesousrep = 0
                if item in self.liste_reco:
                    self.nbfichieratrouver = len(self.liste_reco[item])

                # Parcours des fichiers du répertoire
                for file in liste_lr_transferees:
                    fichier = os.path.join(rep, file)
                    self.logwrite.debug("        Recherche du fichier " + str(file))
                    if os.path.exists(fichier) is True:  # or "generate_maileva.bat" not in file:
                        self.logwrite.debug("        Le fichier existe")
                        try:
                            pdf_sans_ext, _ = aogtools.nom_fichier_sans_extension(file)
                            pattern = re.compile(self.expr_rgx)
                            matcher = pattern.match(pdf_sans_ext)
                            numcontrat = matcher.group(4)
                            self.logwrite.debug("        Resultat regex : " + str(matcher.group(4)) +
                                                " | Num contrat :  " + str(numcontrat))

                            if datelr in self.liste_reco and numcontrat in self.liste_reco[datelr]:
                                self.logwrite.debug(
                                    "        Courrier trouvé pour le contrat " + str(numcontrat) + " : " + str(file))
                                self.liste_reco[datelr].remove(numcontrat)

                                self.nbpdftrouve = self.nbpdftrouve + 1
                                self.nbtrouvesousrep = self.nbtrouvesousrep + 1
                            else:
                                self.logwrite.debug("        Le fichier n'est pas présent dans le répertoire")
                        except Exception as ex:
                            trouve = False
                            for error_item in ["PRIMES", "Thumbs.db", "generate_maileva.bat"]:
                                if error_item in str(file):
                                    trouve = True
                            if trouve is False:
                                self.logwrite.warning("        Except : " + str(ex) + " / " + str(fichier))
                    else:

                        self.logwrite.debug("        Le fichier n'existe pas")

                if self.nbtrouvesousrep > 0:
                    self.logwrite.info("        Nombre de fichiers trouvés : " + str(self.nbtrouvesousrep) + "/" + str(
                        self.nbfichieratrouver))

    def getbodymail(self, body, errors):
        errors = errors + self.nbpdf - self.nbpdftrouve

        self.logwrite.info("Nombre de pdf à rechercher : " + str(self.nbpdf))
        self.logwrite.info("Nombre de pdf trouvés : " + str(self.nbpdftrouve))

        newbody = ""
        newbody = newbody + "Fichier traité : " + os.path.basename(self.datafile) + "\n"
        newbody = newbody + "    Nombre de pdf à rechercher : " + str(self.nbpdf) + "\n"
        newbody = newbody + "    Nombre de pdf trouvés : " + str(self.nbpdftrouve) + "\n"
        if self.nbpdftrouve < self.nbpdf:
            for item in self.liste_reco.copy():
                if len(self.liste_reco[item]) == 0:
                    del self.liste_reco[item]
            self.logwrite.warning("Liste des LR émises sans transmission : ")
            for item in self.liste_reco:
                message = "    " + str(item) + " => " + str(self.liste_reco[item])
                self.logwrite.warning(message)
                newbody = newbody + message + "\n"
        else:
            # Le fichier est complet
            outputdir = os.path.join(repVerifServiceClient, "courriers_recommandes_verification")
            self.logwrite.info("Le fichier est complet, on le déplace dans " + outputdir)

            date_fichier = os.path.basename(self.datafile)[-12:-4]
            annee = date_fichier[0:4]
            mois = date_fichier[4:6]
            print(date_fichier)
            outputdir = os.path.join(outputdir, annee, mois)
            os.makedirs(outputdir, exist_ok=True)

            shutil.copy(self.datafile, outputdir)
            os.remove(self.datafile)

        # print(newbody)
        self.logwrite.info("")
        body = body + newbody + "\n\n"
        return body, errors

    def traitementcomplet(self, liste_lr_transferees):
        self.run(repReco, liste_lr_transferees)
        self.run(repRecoRejet, liste_lr_transferees)
        self.run(repRecoPrecontentieux, liste_lr_transferees)


if __name__ == "__main__":
    body = ""
    errors = 0
    datejour = aogtools.get_time_stamp_aaaammjj().replace("-", "")

    expr_rgx = constants.expr_rgx
    rc = RechercheReco(expr_rgx)

    # Liste des contrats à recherchee
    listetotale = []
    listefichier = []

    for files in os.listdir(repVerifServiceClient):
        if files.endswith(".csv"):
            listefichier.append(os.path.join(repVerifServiceClient, files))

    # sys.exit(0)
    for fichier in listefichier:
        rc.load(fichier, datejour)
        rc.traitementcomplet(listetotale)
        body, errors = rc.getbodymail(body, errors)

    smdir = sendmailtools.Sendmail()
    if errors != 0:
        smdir.set_high_priority()
        # smdir.add_attachment(rc.log.get_log_path())
        subject = "[LR COGEPRINT] Validation des fichiers générés KO"
    else:
        smdir.set_low_priority()
        subject = "[LR COGEPRINT] Validation des fichiers générés OK"
    smdir.set_subject(subject)
    smdir.set_body(body)
    smdir.go_mail()
