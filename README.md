# Introduction 
Scripts utilisés pour traiter les flux vers/ou en provenance des différents partenaires.
Ils sont réapartis par partenaire.
Chaque entité à son propre fichier de configuration, et il y a un fichier de configuration global.

Chaque script `python`, est accompagné d'un fichier `batch (.cmd)` permettant de lancer l'application correctement

La fréquence des traitements est expliquée sur [cette page](https://assurone-group.visualstudio.com/AOG%20-%20Editique/_wiki/wikis/AOG---Editique.wiki/2579/%E2%8F%B0-PLANNING-ORDONNANCEMENT) 

# Pré-requis
- `Python 3.10` pour l'éxécution,
- `Python 2.7` pour l'éxécution HCS
- Package `AOG_DOF_Toolbox` pour toutes les librairires internes

# Scripts

> Les scripts sont répartis par sous répertoire et utilisation

```
├── COGEPRINT
│  ├── LETTRES_CHEQUE
│  ├── RECOMMANDES
│  ├── TERMES
├── DIVERS
├── MAILEVA
├── PREVOIR
│  ├── ONESHOT
│  ├── TERMES
```

---

## COGEPRINT
> Plus d'informations sur [le wiki](https://assurone-group.visualstudio.com/AOG%20-%20Editique/_wiki/wikis/AOG---Editique.wiki/2480/COGEPRINT)

### LETTRES_CHEQUE
> Plus d'informations sur [le wiki](https://assurone-group.visualstudio.com/AOG%20-%20Editique/_wiki/wikis/AOG---Editique.wiki/2513/LETTRES-CHEQUES)

#### traitement_lc_cogeprint.py
> Remontée des lettres chèque pour l'usine courrier

#### traitement_pnd_lc_cogeprint.py
> Remontées des PND lettres chèque pour l'usine courrier

### RECOMMANDES
> Plus d'informations sur [le wiki](https://assurone-group.visualstudio.com/AOG%20-%20Editique/_wiki/wikis/AOG---Editique.wiki/2498/RECOMMANDES)

#### traitement_bdf_recommandes.py
> Remontée des bons de fabrication des courriers recommandés pour l'usine courrier
> 
#### traitement_recommandes.py
> /!\ Traitement quotidien des courriers recommandés.
> Transfert des courriers vers Cogeprint
> Validation du bon envoie des courriers générés par automate
> Envoie d'un recap par mail au Service Qualité Gestion

#### traitement_retour_recommandes.py
> Remontée des accusés de réception/PND des courriers recommandés. Organisation du répertoire de dépot. Et remontée pour l'usine courrier

#### envoie_liste_recommandes.py
> À partir des extractions Cogeprint, retourne les fichiers xlsx mis à jour avec les statuts des courriers recommandés.
Le cas échéant, un zip est généré avec les justificatifs de dépot des courriers.(ex VYV, AXERIA)

### TERMES
> Plus d'informations sur [le wiki](https://assurone-group.visualstudio.com/AOG%20-%20Editique/_wiki/wikis/AOG---Editique.wiki/2471/TERMES)

#### traitement_pnd_termes.py
> Remontée des PND Termes pour l'usine courrier et envoie d'un mail à l'équipe de Remy Herrero

### appsettings.json
> Fichier de paramétrages

---

## DIVERS
### transfert_courrier_divers.py
> Script de composition et de transfert des traitements uniques. Chaque traitement peut nécessiter une adaptation.

### appsettings.json
```json
{
    "composition": {
        "Platform": "D:\\HCS\\Platform\\",
        "opWD": "D:\\referentielEditique",
        "cheminLogos": "D:\\\\ressources\\images\\logos\\",
        "cheminSignatures": "D:\\\\ressources\\images\\signatures\\",
        "cheminParams": "D:\\ressources\\params\\",
        "PYTHONHOME": "C:\\Python27\\",
        "opInstallDir": "D:\\HCS\\Platform\\bin\\backstage\\windows",
        "hcsressourcespath": "D:\\ressources\\hcs\\",
        "opTmpDir": "D:\\travail\\"
    },
    "facturation_psa": {
        "app_name": "facturation_psa",
        "input_dir": "D:\\datas\\FACTURATION_PSA",
        "model_name": "FACTURATION_PSA",
        "transfert": false
    },
    "debug": false,
    "logInfo": "info",
    "ENV": "DEV"
}
```
1. composition
> Paramètres pour la composition via HCS

| **champ**             | **description**                         |
|-----------------------|-----------------------------------------|
| **Platform**          | Localisation de Platform                |
| **opWD**              | Localisation de l'opWD de composition   |
| **cheminLogos**       | Localisation des logos (-DM)            |
| **cheminSignatures**  | Localisation des signature (-DM)        |
| **cheminParams**      | Localisation des fichiers de paramètres |
| **PYTHONHOME**        | Répertoire python à utiliser            |
| **opInstallDir**      | opInstallDir (moteurs)                  |
| **hcsressourcespath** | Localisation des ressources             |
| **opTmpDir**          | Répetoire temp                          |

2. facturation_psa
> Spécifique pour l'application de facturation PSA

| **champ**      | **description**                          |
|----------------|------------------------------------------|
| **app_name**   | Nom de l'application                     |
| **input_dir**  | Localisation des fichiers à traiter      |
| **model_name** | Nom du modèle Designer (lien applis.tab) |
| **transfert**  | transfert FTP auto                       |

---

## PREVOIR
> Plus d'informations sur [le wiki](https://assurone-group.visualstudio.com/AOG%20-%20Editique/_wiki/wikis/AOG---Editique.wiki/2499/COURRIERS-DE-GESTION)
### extractionImpressionJournaliere.py
> /!\ Script de traitement des courriers quotidiens (CP, CV, Gestion, Sinistre)
### gestion_consommables.py
> Script pour envoyer un mail informant de la nécessite de réaliser une commande de consommables.

### TERMES
> Plus d'informations sur [le wiki](https://assurone-group.visualstudio.com/AOG%20-%20Editique/_wiki/wikis/AOG---Editique.wiki/2471/TERMES)
#### transfert_termes_sogessur.py
> /!\ Script de traitement des avis d'échéance sogessur

### appsettings.json
> Fichier de paramétrages
