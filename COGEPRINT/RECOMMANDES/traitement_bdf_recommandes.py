# coding: utf-8

import os
import shutil
import json
from PyPDF2 import PdfFileReader
import AOG.tools as aogtools
import AOG.logvolumetries as volumetries
from openpyxl.comments import Comment

with open(os.path.join(os.path.dirname(__file__), '../appsettings.json')) as json_data:
    config = json.load(json_data)

dirbdf = config["recommandes"]["dirbdf"]


if __name__ == "__main__":
    appname = "TRAITEMENT_LR_BDF_COGEPRINT"
    gc = aogtools.GeneralConfig(appname)

    lv = volumetries.Volumetries("LR COGEPRINT", gc.logwrite, env=gc.env)
    for file in os.listdir(dirbdf):
        # print(file)
        if file.startswith("SUIVI_SANS_AR"):
            suivi_sans_ar_file = os.path.join(dirbdf, file)
            gc.logwrite.info("    Traitement du fichier " + suivi_sans_ar_file)

            bdf = file.replace("SUIVI_SANS_AR_", "BDF_")
            bdf_file = os.path.join(dirbdf, bdf)
            idcommande = bdf.replace(".PDF", "")

            with open(suivi_sans_ar_file, "rb") as f:
                document = PdfFileReader(f, "rb")
                page = document.getNumPages()

                pageObj = document.getPage(0)
                pdftext = pageObj.extractText()

                date_envoie = pdftext.split("\n")[0].strip()
                day = int(str(date_envoie).split("/")[0])
                month = int(str(date_envoie).split("/")[1])
                year = int(str(date_envoie).split("/")[2])

                pageObj = document.getPage(page - 1)
                pdftext = pageObj.extractText()
                for lines in pdftext.split("\n"):
                    if "Nombre d'envois" in lines:
                        nbenvoisposte = int(lines.split(":")[1].replace(" ", ""))
                        break  

                # sys.exit(0)
                lv.ws.cell(month + 3, 1 + day).value = nbenvoisposte
                lv.ws.cell(month + 3, 1 + day).comment = Comment(
                    "Commmande N° : " + str(idcommande), '')
                msg = "Validation de " + str(nbenvoisposte) + " recommandés déposés le " + str(
                    date_envoie)
                gc.logwrite.info("        " + msg)

            outdir = os.path.join(dirbdf, str(year), str(year) + "_" + str(month).zfill(2))
            if os.path.isdir(outdir) is False:
                os.makedirs(outdir)
                gc.logwrite.info("        Création du répertoire " + outdir)

            try:
                gc.logwrite.info("        Copie du fichier " + suivi_sans_ar_file + " vers " + outdir)
                shutil.copy(suivi_sans_ar_file, outdir)
            except PermissionError:
                gc.logwrite.error("Le fichier " + suivi_sans_ar_file + " n'a pas pu être copié vers " + outdir)
                from AOG.sendmailtools import ErrorSendMail
                esm = ErrorSendMail(gc, appname)
                esm.send_error_mail()
                raise

            try:
                gc.logwrite.info("        Copie du fichier " + bdf_file + " vers " + outdir)
                shutil.copy(bdf_file, outdir)
            except PermissionError:
                gc.logwrite.error("Le fichier " + bdf_file + " n'a pas pu être copié vers " + outdir)
                from AOG.sendmailtools import ErrorSendMail
                esm = ErrorSendMail(gc, appname)
                esm.send_error_mail()
                raise

            if gc.is_env_prod():
                gc.logwrite.info("        Suppression du fichier " + suivi_sans_ar_file)
                os.remove(suivi_sans_ar_file)
                gc.logwrite.info("        Suppression du fichier " + bdf_file)
                os.remove(bdf_file)
            else:
                gc.logwrite.warning("        On est sur un environnement NON PROD, on ne supprime pas les fichiers")

    lv.closefile()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
