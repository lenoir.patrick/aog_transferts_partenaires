# coding: utf-8
#   - repReco : 17h10
#   - repRecoRejet : 7h
#   - repRecoPrecontencieux : 16h59 ~17h

import glob
import os
import sys
import shutil
import json
import re

import AOG.pdftools as pdftools
import AOG.sendmailtools as sendmailtools
import AOG.tools as aogtools
import AOG.hcstools as hcstools
import AOG.constants as constants

from AOG.tools import GeneralConfig
from AOG.usine_courrier.usine_courrier import get_produit_from_num_contrat
from recherche_reco_non_envoyes import RechercheReco
import TF.api_request as TFRequest
from xml.dom.minidom import parseString

# Constantes
expr_rgx = constants.expr_rgx

REGEX_PDF = constants.REGEX_PDF
FICHIER_TRANSFERE = constants.FICHIER_TRANSFERE
TEXTE_DONT = constants.TEXTE_DONT

PDF_EXT = constants.PDF_EXT

with open(os.path.join(os.path.dirname(__file__), '../appsettings.json')) as json_data:
    config = json.load(json_data)

repReco = config["recommandes"]["repReco"]
repRecoRejet = config["recommandes"]["repRecoRejet"]
repRecoPrecontentieux = config["recommandes"]["repRecoPrecontentieux"]
repVerifServiceClient = config["recommandes"]["repVerifServiceClient"]
repCogeprint = config["recommandes"]["repCogeprint"]


class TransfertCogeprint(GeneralConfig):
    def __init__(self, appname):
        super().__init__(appname)
        self.appname = appname
        self.datetraitement = aogtools.get_time_stamp_aaaammjj()

        try:
            # BUG 42291
            if self.is_env_prod() is False:
                if os.path.isdir(repCogeprint) is False:
                    os.makedirs(repCogeprint)
                else:
                    for files in os.listdir(repCogeprint):
                        os.remove(os.path.join(repCogeprint, files))
            self.logwrite.info("Copie des fichiers vers : " + repCogeprint)
        except Exception as ex:
            # self.logwrite.error("ERREUR COPIE DE FICHIER : " + reco)
            self.logwrite.error(str(ex))
            smdir = sendmailtools.Sendmail()
            smdir.set_subject("[LR COGEPRINT] Répertoire de sortie non accessible")
            smdir.set_high_priority()
            smdir.add_attachment(self.log.get_log_path())
            body = "Répertoire de sortie non accessible\n    " + repCogeprint
            smdir.set_body(body)
            smdir.go_mail()
            sys.exit(-1)

        self.nbtransfert = 0
        self.nbtransfert_ko = 0
        self.nbtransfert_n_bpages = 0
        self.nb_pages_lot = 1

        self.sm = sendmailtools.Sendmail()

        # Initialisation de l'index commun HCS
        self.indexfile = hcstools.IndFile(self.appname, self.logwrite)
        self.indexfile.init_files()

    # @param
    #       String    date    Date du jour de l'extraction, format AAAA-MM-JJ
    # @Traitement
    #       la fonction va chercher l'ensemble des recommandés dans le repertoire du jour et elle les liste
    # @Return
    #       String[]    Liste des recommandés
    def liste_recommandes(self, date):
        liste_reco = []

        self.logwrite.info("  liste_recommandes : " + repReco + '/' + date + '/*')
        for pdf_reco in glob.glob(repReco + "/" + date + "/*"):
            self.logwrite.debug("Parcours du répertoire : " + pdf_reco)
            if os.path.isfile(pdf_reco):
                pattern = re.compile(REGEX_PDF)
                if pattern.match(os.path.basename(pdf_reco)):
                    liste_reco.append(pdf_reco)
                    self.logwrite.info(FICHIER_TRANSFERE + os.path.basename(pdf_reco))
        return liste_reco

    # @param
    #       String    date    Date du jour de l'extraction, format AAAA-MM-JJ
    # @Traitement
    #       la fonction va chercher l'ensemble des recommandés rejet dans le repertoire du jour et elle les liste
    # @Return
    #       String[]    Liste des recommandés
    def liste_recommandes_rejets(self, date):
        liste_reco = []
        self.logwrite.info("  liste_recommandes_rejets : " + repRecoRejet + '/' + date + '/*')
        for pdf_reco in glob.glob(repRecoRejet + '/' + date + '/*'):
            if os.path.isfile(pdf_reco):
                pattern = re.compile(REGEX_PDF)
                if pattern.match(os.path.basename(pdf_reco)):
                    # on vérifie que le pdf n'est pas le pdf fusionne nomme du genre LRCONTPRIMESAUTOMATE-07-07-2021.pdf
                    pattern = re.compile(".*" + date[8:10] + "-" + date[5:7] + "-" + date[0:4] + "\.[pdf|PDF]")
                    if not pattern.match(os.path.basename(pdf_reco)):
                        liste_reco.append(pdf_reco)
                        self.logwrite.info(FICHIER_TRANSFERE + os.path.basename(pdf_reco))
        return liste_reco

    # @param
    #       String    date    Date du jour de l'extraction, format AAAA-MM-JJ
    # @Traitement
    #       la fonction va chercher l'ensemble des reco precontentieux dans le repertoire du jour et elle les liste
    # @Return
    #       String[]    Liste des recommandes
    def liste_recommandes_precontentieux(self, date):
        liste_reco = []
        self.logwrite.info("  repRecoPrecontentieux : " + repRecoPrecontentieux + '/' + date + '/*')
        for rep_lot in glob.glob(repRecoPrecontentieux + '/' + date + '/*'):
            if os.path.isdir(rep_lot):
                pattern = re.compile("LOT_[0-9]+")
                if pattern.match(os.path.basename(rep_lot)):
                    for pdf_reco in glob.glob(rep_lot + '/*'):
                        if os.path.isfile(pdf_reco):
                            pattern = re.compile(REGEX_PDF)
                            if pattern.match(os.path.basename(pdf_reco)):
                                liste_reco.append(pdf_reco)
                                self.logwrite.info(FICHIER_TRANSFERE + os.path.basename(pdf_reco))

        for pdf_reco in glob.glob(repRecoPrecontentieux + '/' + date + '/*'):
            if os.path.isfile(pdf_reco):
                if os.path.basename(pdf_reco).endswith(PDF_EXT) is True:
                    # On élimine le fichier MAILEVA s'il existe
                    if os.path.basename(pdf_reco).startswith("PRIMES_") is False:
                        liste_reco.append(pdf_reco)
                        self.logwrite.info(FICHIER_TRANSFERE + os.path.basename(pdf_reco))

        return liste_reco

    # @param
    #       String pdf chemin du pdf
    # @Traitement
    #       la fonction retourne le nom du pdf avec le numero de contrat en debut du type
    #       AVANT : \\\\10.128.3.120\\cortex\\depots\\poste\\courriers_recommandes\\LR_IMPAYE_04S255429.pdf
    #       APRES : 04S255429_LR_IMPAYE_04S255429.pdf
    # @Return
    #       String nouveau nom du fichier sans chemin
    @staticmethod
    def pdf_renomme_reco(pdf):
        pdf_sans_ext, _ = aogtools.nom_fichier_sans_extension(pdf)
        pattern = re.compile(expr_rgx)
        matcher = pattern.match(pdf_sans_ext)
        return matcher.group(4) + "_" + pdf_sans_ext, matcher.group(1)

    # @param
    #       String[] fichiers liste des fichiers a transferer depuis Cortex
    # @Traitement
    #       la fonction transfere et renomme les pdf vers le repertoire de partage de Cogeprint
    # @Return
    #       Rien
    def transfertfichiers(self, fichiers, listetotale):
        # Récupération du mot Gratien (St Gratien)
        # pdftools.findspecificwordinpdf(fichiers, "Gratien", self.logwrite)
        nbfichiers = 0
        for reco in fichiers:
            nbpages = pdftools.getnbpagespdf(reco)

            # Si le fichier fait 1 ou 2 pages, il est acceptable
            if nbpages <= 2:
                self.logwrite.debug("transfert fichier : " + os.path.basename(reco))
                try:
                    pdfname, documenttype = self.pdf_renomme_reco(reco)

                    fichier_coge = repCogeprint + "\\" + pdfname + PDF_EXT
                    shutil.copy(reco, fichier_coge)

                    reference_contrat = os.path.basename(fichier_coge).split("_")[0]

                    # Ecriture de l'index HCS au pli
                    infos = self.indexfile.init_infos()
                    infos["NomFichierEntree"] = os.path.basename(reco)
                    infos["NomFichierSortie"] = os.path.basename(fichier_coge)

                    infos["DateTraitement"] = self.datetraitement

                    infos["ReferenceContrat"] = reference_contrat
                    infos["TypeDocCortex"] = documenttype

                    # Recuperation des donnees via l'API
                    donnees_xml = TFRequest.get_response_xml(reference_contrat, "CON", self.logwrite)
                    # On teste si on a un xml vide ou non

                    if donnees_xml is not None and donnees_xml != "":
                        # Creation du parseur et validation du flux XML
                        parser = parseString(donnees_xml)
                        # On instancie le Document DOM
                        data_documents = parser.getElementsByTagName('DataDocumentViewModel')
                        for data_document in data_documents:
                            # On vérifie si le la balise 'numCourtier' contient l'attribut 'i:nil' qui signifie qu'elle est vide
                            if not data_document.getElementsByTagName('CodeCourtier')[0].hasAttribute('i:nil'):
                                infos["Courtier"] = data_document.getElementsByTagName('CodeCourtier')[0].firstChild.data
                            if not data_document.getElementsByTagName('Branche')[0].hasAttribute('i:nil'):
                                infos["Branche"] = data_document.getElementsByTagName('Branche')[0].firstChild.data

                    # Recupération du type de produit
                    infos["Produit"] = get_produit_from_num_contrat(reference_contrat)

                    self.logwrite.info(infos["Produit"])
                    infos["canalDistribution"] = "IMPRESSION"
                    infos["refDistribution"] = "COGEPRINT"
                    infos["nbPages"] = nbpages
                    infos["TypeEnveloppe"] = "ENV-C6"
                    infos["TypePapier"] = "PAPIER-BLANC"
                    self.indexfile.writelinesdoc(nbpages, self.nbtransfert, infos)

                    self.nbtransfert = self.nbtransfert + 1
                    nbfichiers = nbfichiers + 1
                    listetotale.append(os.path.basename(reco))

                except AttributeError as ex:
                    self.logwrite.error("    ERREUR RENOMMAGE FICHIER : " + os.path.basename(reco))
                    self.logwrite.error("    " + str(ex))
                    # self.logwrite.error(str(ex))
                    self.sm.add_attachment(reco)

                    self.nbtransfert_ko = self.nbtransfert_ko + 1
            else:
                self.logwrite.warning("    NB PAGES > 2 (" + str(nbpages) + ") " + os.path.basename(reco))
                # self.logwrite.error(str(ex))
                # self.sm.add_attachment(reco)

                self.nbtransfert_n_bpages = self.nbtransfert_n_bpages + 1
        return nbfichiers, listetotale


# #################################### DEBUT PROGRAMME ####################################
if __name__ == "__main__":
    appname = "COGEPRINT_LR_STD"

    tc = TransfertCogeprint(appname)
    tc.sm.set_to(config['recommandes']['mail_qualite_gestion'])

    listetotale = []

    datejour = aogtools.get_time_stamp_aaaammjj()
    listeRecommandes = tc.liste_recommandes(datejour)
    nbreco, listetotale = tc.transfertfichiers(listeRecommandes, listetotale)

    listeRecommandesRejets = tc.liste_recommandes_rejets(datejour)
    nbrecorejets, listetotale = tc.transfertfichiers(listeRecommandesRejets, listetotale)

    listeRecommandesPrecontentieux = tc.liste_recommandes_precontentieux(datejour)
    nbprecontentieux, listetotale = tc.transfertfichiers(listeRecommandesPrecontentieux, listetotale)

    subject = "[LR] Traitement des recommandés du " + str(datejour)
    body = ""
    subjectfull = ""

    if tc.nbtransfert_ko == 0 and tc.nbtransfert_n_bpages == 0:
        # 47777
        subjectfull = subject
        tc.sm.set_low_priority()
        body = "Traitement des recommandés terminé sans erreur\n"
        # BUG 42291
        if tc.nbtransfert > 0:
            body = body + "    " + str(tc.nbtransfert) + " fichier(s) transféré(s)\n"
            tc.logwrite.info("SVISION" + "    " + str(tc.nbtransfert) + " fichier(s) transféré(s)")
            body = body + TEXTE_DONT + str(nbreco) + " recommandés\n"
            tc.logwrite.info("SVISION" + TEXTE_DONT + str(nbreco) + " recommandés")
            body = body + TEXTE_DONT + str(nbrecorejets) + " recommandés rejets\n"
            tc.logwrite.info("SVISION" + TEXTE_DONT + str(nbrecorejets) + " recommandés rejets")
            body = body + TEXTE_DONT + str(nbprecontentieux) + " precontentieux\n"
            tc.logwrite.info("SVISION" + TEXTE_DONT + str(nbprecontentieux) + " precontentieux")
        else:
            body = body + "Aucun fichier transferré\n"
    else:
        subjectfull = subject + " avec erreur(s)"
        body = "Traitement des recommandés terminé avec erreur(s)\n"
        body = body + "    " + str(tc.nbtransfert) + " fichier(s) transféré(s)\n"
        tc.logwrite.info("SVISION" + "    " + str(tc.nbtransfert) + " fichier(s) transféré(s)")
        body = body + TEXTE_DONT + str(nbreco) + " recommandés\n"
        tc.logwrite.info("SVISION" + TEXTE_DONT + str(nbreco) + " recommandés")
        body = body + TEXTE_DONT + str(nbrecorejets) + " recommandés rejets\n"
        tc.logwrite.info("SVISION" + TEXTE_DONT + str(nbrecorejets) + " recommandés rejets")
        body = body + TEXTE_DONT + str(nbprecontentieux) + " precontentieux\n"
        tc.logwrite.info("SVISION" + TEXTE_DONT + str(nbprecontentieux) + " precontentieux")
        body = body + "\n"
        if tc.nbtransfert_ko > 0:
            body = body + "    " + str(tc.nbtransfert_ko) + " fichier(s) non transféré(s) (car non reconnus)\n"
            tc.logwrite.info("SVISION" + "    " + str(tc.nbtransfert_ko) + " fichier(s) non transféré(s) (car non reconnus)")
        if tc.nbtransfert_n_bpages > 0:
            body = body + "    " + str(
                tc.nbtransfert_n_bpages) + " fichier(s) non transféré(s) (car supérieur à 2 pages)\n"
            tc.logwrite.info("SVISION" + "    " + str(
                tc.nbtransfert_n_bpages) + " fichier(s) non transféré(s) (car supérieur à 2 pages)")

        body = body + "\n"
        body = body + "Les actions à réaliser sont les suivantes :\n"
        if tc.nbtransfert_ko > 0:
            body = body + "- mettre la liste des fichiers acceptables à jour\n"
        if tc.nbtransfert_n_bpages > 0:
            body = body + "- mettre les fichiers à disposition manuellement\n"

        tc.sm.set_high_priority()

    if config['recommandes']['active_verif'] is False:
        tc.logwrite.info("La partie 2 de vérification est désactivée, on quitte le programme")
        tc.sm.set_body(body)
        tc.sm.go_mail()
        sys.exit(0)

    body = body + "\n\n"
    tc.logwrite.info("")
    tc.logwrite.info("DEBUT DE LA PARTIE 2")

    errors = 0

    rc = RechercheReco(expr_rgx, tc.logwrite)

    # Parcours des fichiers en attente de traitement pour récupérer les dates traitées
    rc.get_dates_traitement()

    # Chargement des fichiers à traiter
    for files in os.listdir(repVerifServiceClient):
        completefile = os.path.join(repVerifServiceClient, files)
        if os.path.isfile(completefile) is True and files.endswith(".csv"):
            for name in ["Precontentieux_gestion_", "Precontentieux_primes_", "Contrats_resilies_"]:
                if name in files:
                    datejourfichier = files.split("_")[2].split(".")[0]
                    datejourfichier = datejourfichier[0:4] + "-" + datejourfichier[4:6] + "-" + datejourfichier[6:8]
                    # Si le fichier à traiter est de la prod du jour
                    if datejourfichier == datejour:
                        rc.load(completefile, datejourfichier)

                        if rc.nbpdf > 0:
                            rc.traitementcomplet(listetotale)
                        body, errors = rc.getbodymail(body, errors)

    tc.sm.add_attachment(tc.log.get_log_path())
    if errors != 0:
        subjectfull = subject + " avec erreur(s)"
        tc.sm.set_high_priority()

    tc.sm.set_subject(subjectfull)
    tc.sm.set_body(body)
    tc.sm.go_mail()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
