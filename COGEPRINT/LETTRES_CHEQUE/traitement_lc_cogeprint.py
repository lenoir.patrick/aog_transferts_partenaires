# coding: utf-8
import json
import os

import time
import datetime

import AOG.tools as aogtools
import AOG.hcstools as hcstools
import AOG.sendmailtools as sendmailtools

with open(os.path.join(os.path.dirname(__file__), '..', 'appsettings.json')) as json_data:
    config = json.load(json_data)


if __name__ == "__main__":

    appname = "COGEPRINT_LC_STD"

    gc = aogtools.GeneralConfig(appname)

    # Initialisation de l'index commun HCS
    # indexfile = hcstools.IndFile(appname, logwrite)
    # indexfile.init_files()

    # Parcours du répertoire des fichiers de data des LC
    dateprod = datetime.date.today()

    repLC = config['lettres_cheques']["repLC"]
    repLC = os.path.join(repLC, str(dateprod.year).zfill(4), str(dateprod.year).zfill(4) + str(dateprod.month).zfill(2))
    gc.logwrite.info("    Répertoire de traitement : " + repLC)
    nbdoc = 0
    for files in os.listdir(repLC):
        if files.startswith("AOG_RLC_"):
            file = os.path.join(repLC, files)
            with open(file, "r") as openfile:
                gc.logwrite.info("    Traitement du fichier : " + files)
                indexfile = hcstools.IndFile(appname, gc.logwrite)
                indexfile.init_files()
                for line in openfile:
                    tmpline = line.strip().split(";")
                    IDENTIFIANT_PAIEMENT = tmpline[0]
                    REFERENCE_COMPTABLE = tmpline[1]
                    DATE_FICHIER = tmpline[2][0:4] + "-" + tmpline[2][4:6] + "-" + tmpline[2][6:8]
                    DATE_MISE_POSTE = tmpline[3][0:4] + "-" + tmpline[3][4:6] + "-" + tmpline[3][6:8]

                    infos = indexfile.init_infos()
                    infos["NomFichierEntree"] = files.replace("RLC", "ILC")
                    infos["NomFichierSortie"] = files
                    infos["DateTraitement"] = DATE_FICHIER
                    infos["DateImpression"] = DATE_MISE_POSTE
                    infos["ReferenceContrat"] = IDENTIFIANT_PAIEMENT
                    infos["TypeDocCortex"] = "LC"

                    infos["canalDistribution"] = "IMPRESSION"
                    infos["refDistribution"] = "COGEPRINT"
                    infos["nbPages"] = 1
                    infos["nbFeuilles"] = 1
                    infos["TypeEnveloppe"] = "ENV-C6"
                    infos["TypePapier"] = "PAPIER-LC"
                    infos["uuidpli"] = "COGEPRINT_LC_" + IDENTIFIANT_PAIEMENT

                    # Ecriture des informations du pli
                    indexfile.writelinesdoc(1, nbdoc, infos)
                    nbdoc = nbdoc + 1
                    # sys.exit(0)
                time.sleep(1)

    # Envoie du mail d'affranchissement
    mailrecap = sendmailtools.Sendmail()
    mailrecap.set_subject("[LC] Remontée des Lettres Cheques")
    body = "La remontée des LETTRE CHEQUE s'est effecutée avec succès.\n\n"
    body = body + "   " + str(nbdoc) + " ont été transmis\n\n"
    body = body + "\n"
    body = body + "L'équipe éditique"
    mailrecap.set_body(body)
    mailrecap.add_attachment(gc.log.get_log_path())
    mailrecap.set_low_priority()
    mailrecap.go_mail()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
