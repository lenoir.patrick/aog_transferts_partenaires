``` json
{
    "dirs": {
        "rep_pdf": "\\\\10.128.3.120\\cortex\\prod\\pdf",
        "rep_adm": "ADM",
        "rep_grdt": "GRDT",
        "rep_flotte": "FLOTTE",
        "rep_constructeurs": "AUTO\\Constructeurs",
        "rep_lot_Blanc": "DEMANDE_IMPRESSION\\prevoir",
        "rep_grdt_auto": "AUTO",
        "rep_grdt_auto_tempo": "AUTO_TEMPO",
        "rep_grdt_ccar": "CCAR",
        "rep_grdt_cyclo50": "CYCLO",
        "rep_grdt_moto": "MOTO",
        "rep_grdt_vsp": "VSP",
        "outputdirPREVOIR": "\\\\10.128.3.120\\cortex\\prod\\pdf\\ADM\\DEMANDE_IMPRESSION\\prevoir"
    },
    "composition":{
        "Platform": "D:\\HCS\\Platform\\",
        "opWD": "D:\\referentielEditique",
        "cheminLogos": "D:\\\\ressources\\images\\logos\\",
        "cheminSignatures": "D:\\\\ressources\\images\\signatures\\",
        "cheminParams": "D:\\ressources\\params\\",
        "PYTHONHOME": "C:\\Python27\\",
        "opInstallDir": "D:\\HCS\\Platform\\bin\\backstage\\windows",
        "hcsressourcespath": "D:\\ressources\\hcs\\",
        "opTmpDir": "D:\\travail\\"
	},
	"termes_sogessur": {
        "indir": "\\\\A1-SRV-05\\Sauvegardes\\Extractions_CORTEX\\TERMES\\TERMES CORTEX\\SOGESSUR\\Avis_Echeance_SOGESSUR"
    },
    "picage": {
        "active": false,
        "nombre": 10,
        "liste": [
            "CVDEFINITIVE",
            "CP",
            "CVPROV",
            "ECHEAN",
            "RI",
            "ENVOI_PV_CONSTAT",
            "AVRESILIATION",
            "CVFACS",
            "SINLETTRETYPE",
            "ECHEANCIER",
            "DEVIS",
            "AVLA",
            "ATT_TEMP",
            "ATTPROV",
            "ATT-MULTI",
            "MULTI_PRO",
            "CP-MULTI",
            "DOCUMENT_ASSURANCES",
            "SINATTNALCO",
            "SIN_DEC_CIRC_ROULANT",
            "FICHESOUSRECENTE",
            "FIC",
            "PARC",
            "SINASSNONDEC",
            "CLIENT_REFUS_HAMON",
            "RELANCE_PIECE_MANQUANTE"
        ]
    },
    "dontprint": [
        "CVFACS",
        "ATTPROV",
        "SIN_DEC_CIRC_ROULANT",
        "MANDAT_SEPA_COMMUN",
        "SANT_PRVR_TAB_GAR_COMMUN",
        "SANT_EQ_TAB_GAR_COMMUN",
        "RI-FLOTTE"
    ],
    "validationimpression": [
        "CVFACS",
        "ATT_TEMP",
        "ATTPROV",
        "CP-MULTI",
        "DOCUMENT_ASSURANCES",
        "SINATTNALCO",
        "FICHESOUSRECENTE",
        "PARC"
    ],
    "debug": false,
    "logInfo": "info",
    "ENV": "DEV"
}
```

| champ               | description                                                            |
|---------------------|------------------------------------------------------------------------|
| dontprint           | ypes de documents à ne pas envoyer en impression                       |
| validatinimpression | Types de documents à envoyer par mail pour validation avant impression |
| debug               | debug activé (true/false)                                              |
| loginfo             | exhaustivité des logs (error, warning, info, debug)                    |
| env                 | type d'environnement (DEV, PPROD, PROD)                                |


# dirs
| champ               | description                                                    |
|---------------------|----------------------------------------------------------------|
| rep_pdf             | Répertoire racine des PDF                                      |
| rep_adm             | Répertoire divers                                              |
| rep_grdt            | Répertoire pour AssurOnline                                    |
| rep_flotte          | Répertoire pour les documents FLOTTE                           |
| rep_constructeurs   | Répertoire pour les documents constructeurs                    |
| rep_lot_Blanc       | Répertoire pour les documents générés par automate TSC         |
| rep_grdt_auto       | Not used                                                       |
| rep_grdt_auto_tempo | Not used                                                       |
| rep_grdt_ccar       | Not used                                                       |
| rep_grdt_cyclo50    | Not used                                                       |
| rep_grdt_moto       | Not used                                                       |
| rep_grdt_vsp        | Not used                                                       |
| outputdirPREVOIR    | Répertoire racine de sortie pour les zip à générer et archiver |

# composition
Paramétrage d'appel aux moteurs Sefas HCS

| champ             | description                                    |
|-------------------|------------------------------------------------|
| Platform          | Positionnement des moteurs HCS                 |
| opWD              | Positionnement de l'opWD de composition        |
| cheminLogos       | Positionnement des logos dynamiques            |
| cheminSignatures  | Positionnement des signatures dynamiques       |
| cheminParams      | Localisation du paramétrage spécifique HCS     |
| PYTHONHOME        | Répertoire Python utilisé pour les composition |
| opInstallDir      | opInstallDir pour les appels moteurs           |
| hcsressourcespath | Répertoire racine des ressources               |
| opTmpDir          | opTmpDir pour les appels moteur                |

# termes_sogessur
| champ | description                         |
|-------|-------------------------------------|
| indir | Localisation des fichiers à traiter |

# picage
Pour automatiser l'envoie de picage aléatoire de vérification à chaque production

| champ  | description                              |
|--------|------------------------------------------|
| active | mode actif ou non (true/false)           |
| nombre | nombre de documents à envoyer            |
| liste  | Liste des types de documents à récupérer |

