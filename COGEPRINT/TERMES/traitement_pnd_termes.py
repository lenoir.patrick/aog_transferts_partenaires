# coding: utf-8
import shutil
import time
import datetime
import os
import json

import AOG.tools as aogtools
import AOG.usine_courrier.usine_courrier as UC
from AOG.sendmailtools import Sendmail
from AOG.usine_courrier.usine_courrier import get_pnd, get_type_pnd

with open(os.path.join(os.path.dirname(__file__), '..', 'appsettings.json')) as json_data:
    config = json.load(json_data)

if __name__ == "__main__":
    appname = "PND_COGEPRINT_TERMES"
    gc = aogtools.GeneralConfig(appname)

    # J-1 format %Y-%m-%d
    dateprod = datetime.date.today()
    dirpnd = config['pnd']["dirpnd"]
    gc.logwrite.info("    Répertoire de traitement : " + dirpnd)

    # Creation de l'email
    gc.logwrite.info("    Préparation du mail")
    smpnd = Sendmail()
    smpnd.set_subject("[TERMES PND] Traitement du " + str(dateprod))
    smpnd.set_to(config['pnd']['mailpnd'])
    body = "Ceci est un mail automatique.\n\n"

    prixpnd = get_pnd(appname)

    nbFichiers = 0

    use_usinecourrier = True
    try:
        # Parcours du repertoire
        for file in os.listdir(dirpnd):
            csvfile = os.path.join(dirpnd, file)

            # Test de chaque fichier trouve
            if os.path.isfile(csvfile):
                if use_usinecourrier is True:
                    usine_courrier = UC.UsineCourrier(gc.logwrite, appname + "_" + file)
                # Récupération de la période de retour
                ti_m = os.path.getmtime(csvfile)
                m_ti = time.ctime(ti_m)
                t_obj = time.strptime(m_ti)
                # Transforming the time object to a timestamp
                # of ISO 8601 format
                T_stamp = time.strftime("%Y-%m-%d %H:%M:%S", t_obj)
                periode = T_stamp[0:4] + "_" + T_stamp[5:7]

                if nbFichiers == 0:
                    gc.logwrite.info("    Période du fichier : " + periode)
                    body = body + "Voici ci-joint le(s) fichier(s) de remontées de plis en PND de la semaine" + "\n"
                gc.logwrite.info("    Fichier trouvé : " + file)

                sqlblock = ""
                listindex = open(csvfile, "r")
                idx = 1
                for line in listindex:
                    # print(line)
                    motif_pnd = line.split(";")[3]
                    numcontrat = line.split(";")[6]

                    type_pnd = get_type_pnd(motif_pnd)

                    gc.logwrite.info("Enregistrement pour : " + numcontrat)
                    idx = idx + 1
                    if use_usinecourrier is True:
                        # usine_courrier.set_pnd(numcontrat, appname)
                        sql = "UPDATE usine_courriers_editique.usine_courrier as UC" + \
                              " JOIN usine_courriers_editique.production_papier as PP ON UC.production_papier_ID = PP.ID" + \
                              " SET UC.pnd = " + str(type_pnd) + ", UC.pxpnd = " + str(
                            prixpnd) + ", PP.nbpnd = PP.nbpnd + 1, PP.cout_PND = PP.cout_PND + " + str(prixpnd) + \
                              " WHERE PP.type = 'TERMES' AND UC.ReferenceContrat = '%" + numcontrat + "%' and UC.TypeDocCortex = 'AVISECHEAN' and UC.pnd = 0;"

                        usine_courrier.set_sql_lot(sql)
                listindex.close()

                # if use_usinecourrier is True:
                #     usine_courrier.commit()
                # Ajout du fichier en PJ
                smpnd.add_attachment(csvfile)
                gc.logwrite.info("        Ajout de de la pièce jointe")

                # Archivage du fichier uniquement en production
                if gc.env == "PROD":
                    sousdir = os.path.join(dirpnd, periode)
                    os.makedirs(sousdir, exist_ok=True)

                    try:
                        shutil.move(csvfile, sousdir)
                    except (Exception,):
                        os.remove(os.path.join(sousdir, os.path.basename(csvfile)))
                        shutil.move(csvfile, sousdir)
                    gc.logwrite.info("        Archivage du fichier")

                nbFichiers = nbFichiers + 1

        if nbFichiers == 0:
            body = body + "Pas de fichier à traiter cette semaine"

        # Envoie du mail
        smpnd.set_body(body)
        smpnd.go_mail()
        gc.logwrite.info("Envoie du mail")

    except Exception as ex:
        gc.logwrite.error(str(ex))
        gc.logwrite.error("== TRAITEMENT TERMINE AVEC ERREUR ==")
        smpnd = Sendmail()
        smpnd.set_subject("[TERMES PND] Erreur lors du traitement des remontées")
        smpnd.set_to(config['pnd']['mailpnd'])
        body = "Ceci est un mail automatique.\n\n"
        body = body + "Une erreur est survenue lors du traitement des PND des Termes" + "\n"
        body = body + "Voir la log jointe" + "\n"
        smpnd.set_body(body)
        smpnd.set_high_priority()
        smpnd.add_attachment(gc.log.get_log_path())
        smpnd.go_mail()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
