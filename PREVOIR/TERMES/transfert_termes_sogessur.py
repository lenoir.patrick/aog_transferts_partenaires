#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import AOG.pdftools as pdftools
import AOG.tools as aogtools

import AOG.sendmailtools as sendmailtools
import AOG.transferttools as transferttools
import AOG.logvolumetries as volumetries
import AOG.hcstools as hcstools

from PyPDF2 import PdfFileWriter, PdfFileReader
import os
import time
import json
import zipfile


if __name__ == "__main__":
    with open(os.path.join(os.path.dirname(__file__), '../appsettings.json')) as json_data:
        config = json.load(json_data)

    appname = "PREVOIR_TERMES_SOGESSUR"
    gc = aogtools.GeneralConfig(appname)

    indir = config["termes_sogessur"]["indir"]
    workingdir = aogtools.get_working_directory(appname)
    gc.logwrite.info("Répertoire de travail : " + workingdir)
    ts = time.gmtime()
    ts = time.strftime("%Y%m%d", ts)


    nbdoc = 1
    numFeuilles = 1

    # Initialisation de l'index commun HCS
    indexfile = hcstools.IndFile(appname, gc.logwrite)
    indexfile.init_files()
    gc.logwrite.info("Vérification du répertoire : " + indir)
    for files in os.listdir(indir):
        file = os.path.join(indir, files)
        if os.path.isfile(file) is True and file.endswith(".pdf"):
            gc.logwrite.info("Traitement du fichier " + file)

            typefichier = os.path.basename(file).split("_")

            typetrt = typefichier[3]
            datetrt = typefichier[4].split(".")[0]
            """datetrt_mois = datetrt.split("-")[0]
            datetrt_annee = datetrt.split("-")[1]"""
            datetrt_mois = str(ts)[4:6]
            datetrt_annee = str(ts)[0:4]

            gc.logwrite.info(str(typefichier))
            nbpages = pdftools.getnbpagespdf(file)
            gc.logwrite.info("    " + str(nbpages) + " pages soit " + str(int(nbpages/2)) + " plis")
            input_pdf = PdfFileReader(file)

            gc.logwrite.debug("        Document " + str(typefichier))
            for i in range(0, nbpages, 2):
                output = PdfFileWriter()

                page_recto = input_pdf.getPage(i)
                page_verso = input_pdf.getPage(i + 1)

                # Recherche du numéro de contrat pour ne pas avoir de pb de nommage de fichier
                Text = page_recto.extractText()
                occ = Text.find("Contrat n°")
                if occ != -1:
                    if typetrt == "MRPRO":
                        numContrat = Text[occ + 10:occ + 26]
                        if numContrat[0:4] == "1422":
                            numContrat = numContrat[0:15]
                    elif typetrt == "RDD":
                        numContrat = Text[occ + 10:occ + 25]
                    else:
                        numContrat = str(nbdoc).zfill(8)
                        gc.logwrite.warning("    Numéro de contrat non reconnu pour le fichier " + os.path.basename(file))
                        gc.logwrite.warning("    Utilisation de l'index : " + numContrat)
                else:
                    numContrat = str(nbdoc).zfill(8)
                    gc.logwrite.warning("    Numéro de contrat non reconnu pour le fichier " + os.path.basename(file))
                    gc.logwrite.warning("    Utilisation de l'index : " + numContrat)

                output.addPage(page_recto)
                output.addPage(page_verso)
                filename = typefichier[0] + "_" + typefichier[1] + "-" + typefichier[2] + "_" + typefichier[3] + \
                           "-" + numContrat + "-00-VIDE-VIDE-" + str(ts) + "-0.pdf"
                with open(os.path.join(workingdir, filename), "wb") as output_stream:
                    output.write(output_stream)

                # Ecriture de l'index HCS au pli
                infos = indexfile.init_infos()
                infos["NomFichierEntree"] = os.path.basename(file)
                infos["NomFichierSortie"] = filename
                infos["DateTraitement"] = datetrt_annee + "-" + datetrt_mois + "-28"
                infos["DateImpression"] = datetrt_annee + "-" + datetrt_mois + "-28"
                infos["ReferenceContrat"] = numContrat
                infos["Branche"] = typetrt
                infos["Marque"] = typefichier[2]
                infos["TypeDocCortex"] = "AVISECHEAN"
                infos["canalDistribution"] = "IMPRESSION"
                infos["refDistribution"] = "PREVOIR"
                infos["nbPages"] = 2
                infos["nbFeuilles"] = 1
                infos["TypeEnveloppe"] = "ENV-C5"
                infos["TypePapier"] = "PAPIER-BLANC"
                indexfile.writelinesdoc(2, nbdoc - 1, infos)

                nbdoc = nbdoc + 1
                numFeuilles = numFeuilles + 2

            if typetrt == "MRPRO":
                ligne_excel = 9
            elif typetrt == "RDD":
                ligne_excel = 10
            lv = volumetries.Volumetries("TERMES", gc.logwrite, year=datetrt_annee, env=gc.env)
            lv.ws.cell(int(ligne_excel), int(datetrt_mois) + 2).value = int(nbpages / 2)
            gc.logwrite.info("Valeurs : " + str(ligne_excel) + ":" + str(int(datetrt_mois) + 2) +
                          " - " + str(int(nbpages / 2)))
            lv.closefile()

    nbdocs = 0
    listeplis = []
    zipnamePREVOIR = os.path.join(workingdir, "ASSURONE_AE_SOGESSUR_" + str(ts) + ".zip")
    zipPREVOIR = zipfile.ZipFile(zipnamePREVOIR, "w")
    gc.logwrite.info("Ouverture du fichier zip : " + zipnamePREVOIR)
    # logwrite.info("Ouverture du fichier zip : " + workingdir)
    for file in os.listdir(workingdir):
        if file.endswith(".pdf") is True:
            filePath = os.path.join(workingdir, file)
            gc.logwrite.debug("      Ajout du fichier : " + file)
            zipPREVOIR.write(filePath, os.path.basename(filePath))
            os.remove(filePath)
            nbdocs = nbdocs + 1
    gc.logwrite.info("Fermeture du fichier zip : " + zipnamePREVOIR)
    zipPREVOIR.close()

    # Upload des fichiers vers Prevoir
    upload = transferttools.upload_ftp(workingdir, gc.logwrite, gc.is_debug_mode(), nbdocs)
    if gc.env == "PROD":
        gc.logwrite.info("Début de l'étape de transfert FTP")
        upload.uploadfiles()
    else:
        gc.logwrite.warning("Environnement non PROD => Pas de transfert FTP")
        upload.uploadfiles(False)
    gc.logwrite.info("Fin de l'étape de transfert FTP")

    gc.logwrite.info("Fin du traitement")

    # Envoie du mail d'affranchissement
    mailrecap = sendmailtools.Sendmail()
    mailrecap.set_subject("Transfert des AE SOGESSUR vers PREVOIR OK")
    body = "Le transfert des documents vers PREVOIR s'est déroulé sans erreur.\n\n"
    body = body + "   " + str(nbdocs) + " plis ont été transmis\n\n"
    body = body + "\n"
    body = body + "L'équipe éditique"
    mailrecap.set_body(body)
    mailrecap.add_attachment(gc.log.get_log_path())
    mailrecap.set_low_priority()
    mailrecap.go_mail()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)