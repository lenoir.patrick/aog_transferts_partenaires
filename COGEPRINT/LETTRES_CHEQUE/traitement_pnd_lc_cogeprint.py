# coding: utf-8
import json
import os
import datetime

import AOG.tools as aogtools
import AOG.sendmailtools as sendmailtools
import AOG.usine_courrier.usine_courrier as UsineCourrier
from AOG.usine_courrier.usine_courrier import get_pnd

with open(os.path.join(os.path.dirname(__file__), '..', 'appsettings.json')) as json_data:
    config = json.load(json_data)

if __name__ == "__main__":

    appname = "PND_COGEPRINT_LC"
    gc = aogtools.GeneralConfig(appname)

    # Parcours du répertoire des fichiers de data des LC
    dateprod = datetime.date.today()

    repLC = config['lettres_cheques']["repPND"]
    repLC = os.path.join(repLC, str(dateprod.year).zfill(4), str(dateprod.year).zfill(4) + str(dateprod.month).zfill(2))
    gc.logwrite.info("    Répertoire de traitement : " + repLC)
    nbdoc = 0

    for files in os.listdir(repLC):
        if files.startswith("AOG_RPND"):
            usine_courrier = UsineCourrier.UsineCourrier(gc.logwrite, appname + "_" + files)
            file = os.path.join(repLC, files)
            with open(file, "r") as openfile:
                # print(file)
                gc.logwrite.info("    Traitement du fichier : " + files)
                for detail in openfile:
                    if detail.startswith("IDENTIFIANT") is False:
                        line = ""
                        nbdoc = nbdoc + 1

                        # Recupération de la surtaxe de gestion du pnd
                        prixpnd = get_pnd(appname)

                        sql = "UPDATE usine_courriers_editique.usine_courrier as UC" + \
                              " JOIN usine_courriers_editique.production_papier as PP ON UC.production_papier_ID = PP.ID" + \
                              " SET UC.pnd = true, UC.pxpnd = " + str(prixpnd) + ", PP.nbpnd = PP.nbpnd + 1, PP.cout_PND = PP.cout_PND + " + str(prixpnd) + \
                              " WHERE UC.TypeDocCortex = 'LC' AND UC.ReferenceContrat = '" + detail.split(";")[0] + "' and UC.pnd = 0;"
                        usine_courrier.set_sql_lot(sql)

    gc.logwrite.info("Nombre de PNDs : " + str(nbdoc))

    # Envoie du mail d'affranchissement
    mailrecap = sendmailtools.Sendmail()
    mailrecap.set_subject("[LC] Remontée des PNDs Lettres Chèque")
    body = "La remontée des PNDs LETTRE CHEQUE s'est effectué avec succès.\n\n"
    body = body + "   " + str(nbdoc) + " ont été transmis\n\n"
    body = body + "\n"
    body = body + "L'équipe éditique"
    mailrecap.set_body(body)
    mailrecap.add_attachment(gc.log.get_log_path())
    mailrecap.set_low_priority()
    mailrecap.go_mail()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
