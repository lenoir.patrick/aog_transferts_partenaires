# coding: utf-8
import json
import os
import AOG.pdftools as pdftools

with open(os.path.join(os.path.dirname(__file__), 'appsettings.json')) as json_data:
    config = json.load(json_data)

dirout = os.path.join(config["dirs"]["outputdirPREVOIR"])

if __name__ == "__main__":
    result = os.path.join(os.path.dirname(__file__), "result.txt")
    listresult = open(result, "w")
    listedoc = []

    for dir in os.listdir(dirout):
        dircomplet = os.path.join(dirout, dir)

        for file in os.listdir(dircomplet):
            if file.endswith(".pdf"):
                typedoc = file.split("-")[0]
                if typedoc not in listedoc or typedoc != "AVLA":
                    pdffile = os.path.join(dircomplet, file)
                    trouve = pdftools.contain_specific_word(pdffile, "Gratien")
                    if trouve is True:
                        listresult.write(pdffile + "\n")
                        listresult.flush()
                        if typedoc not in listedoc:
                            listedoc.append(typedoc)

                        #print("   " + str(listedoc))
                        print("   " + pdffile)
    print(listedoc)