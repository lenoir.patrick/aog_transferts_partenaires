# coding: utf-8

import os
import shutil
import json
from PyPDF2 import PdfFileReader
import re
import AOG.tools as aogtools
from AOG.tools import GeneralError
from AOG.sendmailtools import Sendmail
import AOG.usine_courrier.usine_courrier as UsineCourrier
from AOG.usine_courrier.usine_courrier import get_pnd, get_type_pnd

with open(os.path.join(os.path.dirname(__file__), '../appsettings.json')) as json_data:
    config = json.load(json_data)

dirretourpdf = config["recommandes"]["dirretourpdf"]


def copy_pdf_file_global(pdffile, dirretourpdf, num_contrat, suffixe, matcher, extractdir=None):
    # Creation du répertoire de sortie
    subdir = os.path.join(dirretourpdf, file[0:9])
    if os.path.isdir(subdir) is False:
        os.makedirs(subdir)
        gc.logwrite.info("Création du répertoire : " + subdir)

    # Copie du fichier d'extraction
    if matcher is not None:
        gc.logwrite.info("Reco isolé pour le partenaire : " +
                         os.path.basename(extractdir) + " | " + file + " | " + num_contrat)
        copy_pdf_file(pdffile, extractdir, num_contrat, suffixe)

    # Copie du fichier d'archivage
    copy_pdf_file(pdffile, subdir, num_contrat, suffixe)

    '''
    ### A ACTIVER SI BESOIN D'ISOLER LES RECO QUI UN PB DE RECONNAISSANCE DE STATUT
    if suffixe in ["ANOMALIE", "INCONNU"]:
        anosdir = os.path.join(dirretourpdf, suffixe)
        if os.path.isdir(anosdir) is False:
            os.makedirs(anosdir)
        copy_pdf_file(pdffile, subdir, num_contrat, suffixe)
        gc.logwrite.warning("Reco avec le statut " + suffixe + " : " + file + " | " + num_contrat)
    else:
        gc.logwrite.debug("Reco avec le statut " + suffixe + " : " + file + " | " + num_contrat)
    '''


def copy_pdf_file(pdffile, copydir, num_contrat, suffixe):
    outfile = os.path.join(copydir, file.replace(".pdf", "_" + num_contrat + "_" + suffixe + ".pdf"))
    if os.path.isfile(outfile) is False:
        shutil.copy(pdffile, outfile)


if __name__ == "__main__":
    try:
        appname = "PND_COGEPRINT_LR"
        appname_config = "COGEPRINT_LR"

        gc = aogtools.GeneralConfig(appname)
        listeVYV = {}

        usine_courrier = UsineCourrier.UsineCourrier(gc.logwrite, appname + "_" + aogtools.get_time_stamp_aaaammjj())

        for file in os.listdir(dirretourpdf):
            pdffile = os.path.join(dirretourpdf, file)
            if os.path.isfile(pdffile) is True and pdffile.endswith(".pdf") is True:
                # Creation du répertoire de sortie
                subdir = os.path.join(dirretourpdf, file[0:9])

                if os.path.isdir(subdir) is False:
                    gc.logwrite.info("Création du sous répertoire : " + file[0:9])
                os.makedirs(subdir, exist_ok=True)

                # os.remove(pdffile)
                # sys.exit(0)
                # Ouverture / lecture du fichier PDF

                item = []
                extractdir = None
                suffixe = None
                matcher = None
                isVYV = False

                with open(pdffile, 'rb') as f:
                    numessai = 1
                    while numessai <= 2 and suffixe is None:
                        pdfDoc = PdfFileReader(f)
                        pageObj = pdfDoc.getPage(pdfDoc.numPages - numessai)
                        pdftext = pageObj.extractText().replace('\n', '')

                        num_reco = pdftext.find("Recommandé n")
                        num_reco = pdftext[num_reco + 14:num_reco + 30].split(" ")[0]

                        num_contrat = pdftext.find("Référence")
                        num_contrat = pdftext[num_contrat + 12:num_contrat + 30].split(" ")[0]

                        # On cherche les cas spécifiques pour les partenaires
                        for item in config["recommandes"]["retour_recommandes"]["clients"]:
                            # Une extraction spécifique est-elle nécessaire ?
                            if item["active"] is True:
                                exprRGX = "^(" + num_contrat + ")"
                                pattern = re.compile(item["filtreregex"])
                                matcher = pattern.match(num_contrat)

                                if matcher is not None:
                                    extractdir = os.path.join(dirretourpdf, item["nom"])
                                    os.makedirs(extractdir, exist_ok=True)

                                    if item["nom"] == "VYV":
                                        isVYV = True
                                    break

                        # PND/NPAI
                        if re.search("PND:", pdftext):
                            suffixe = "PND"
                            pageObj = pdfDoc.getPage(pdfDoc.numPages - 1)
                            pdftext = pageObj.extractText().replace('\n', '')
                            motif_pnd = pdftext.find("pour cause d")
                            motif_pnd = pdftext[motif_pnd + 12:]
                            motif_pnd = motif_pnd[0:100]
                            motif_pnd = get_type_pnd(motif_pnd)

                        # DISTRIBUES
                        elif re.search("Distribué:", pdftext):
                            suffixe = "DISTRIBUE"

                        # DISTRIBUES AVEC REQUETE ET RETOUR POSTE
                        elif re.search("ATTESTATION DE DISTRIBUTION", pdftext) or \
                                re.search("distribué {2}en {2}date {2}du", pdftext) or \
                                re.search("distribué en date du", pdftext):
                            suffixe = "DISTRIBUE"
                            pageObj = pdfDoc.getPage(pdfDoc.numPages - 2)
                            pdftext = pageObj.extractText().replace('\n', '')
                            num_contrat = pdftext.find("Référence")
                            num_contrat = pdftext[num_contrat + 12:num_contrat + 30].split(" ")[0]

                        # ANOMALIES ??
                        elif re.search("En Cours:", pdftext) and numessai == 1:
                            suffixe = "ANOMALIE"

                        # INCONNU ??
                        else:
                            if numessai == 1:
                                gc.logwrite.warning("Reco non déterminable au premier essai : " + file)
                                numessai = 2
                            else:
                                gc.logwrite.warning("    Reco non déterminable au deuxième essai")
                                gc.logwrite.warning("    On le défini comme statut inconnu")
                                suffixe = "INCONNU"

                # Un sufixe a été trouvé
                if suffixe != "":
                    copy_pdf_file_global(pdffile, dirretourpdf, num_contrat, suffixe, matcher, extractdir)

                    if suffixe == "PND":
                        sql = "UPDATE usine_courriers_editique.usine_courrier as UC" + \
                              " JOIN usine_courriers_editique.production_papier as PP ON UC.production_papier_ID = PP.ID" + \
                              " SET UC.recommande = '" + num_reco + "', UC.pnd = " + str(
                            motif_pnd) + ", PP.nbpnd = PP.nbpnd + 1 " + \
                              " WHERE PP.type = 'LR' AND UC.ReferenceContrat = '" + num_contrat + "' and UC.pnd = 0;"
                        usine_courrier.set_sql_lot(sql)

                    elif suffixe == "DISTRIBUE":
                        sql = "UPDATE usine_courriers_editique.usine_courrier as UC" + \
                              " JOIN usine_courriers_editique.production_papier as PP ON UC.production_papier_ID = PP.ID" + \
                              " SET UC.recommande = '" + num_reco + "', UC.pnd = 6 WHERE PP.type = 'LR' AND UC.ReferenceContrat = '%" + num_contrat + "%' and UC.pnd = 0;"
                        usine_courrier.set_sql_lot(sql)

                    if gc.env == "PROD":
                        try:
                            os.remove(pdffile)
                        except (Exception,):
                            pass
                    # os.remove(pdffile)
                    if isVYV is True:
                        infoscontrat = {'nom': os.path.basename(pdffile), 'num_contrat': num_contrat, 'statut': suffixe}
                        listeVYV[num_contrat] = infoscontrat

        if listeVYV != {}:
            gc.logwrite.info("Préparation du mail pour VYV")

            body = ""
            for item in listeVYV:
                body = body + "Recommandé : " + listeVYV[item]['nom'].replace(".pdf", "") + "\t" + "Contrat : " + \
                       listeVYV[item]['num_contrat'].ljust(15) + "\t" + "Statut : " + listeVYV[item]['statut'] + "\n"

            sm = Sendmail()
            sm.set_subject("[VYV PND] Liste des retours du " + aogtools.get_time_stamp_aaaammjj())
            sm.set_to(config["pnd"]["mailpnvyv"])
            sm.set_body(body)
            sm.go_mail()
            gc.logwrite.info("Envoie du mail pour VYV")
    except Exception as ex:
        gc.logwrite.error(ex)
        ge = GeneralError("ERREUR GENERALE : " + str(ex))
        ge.set_params(appname, gc.log.get_log_path())
        ge.send_error_mail()

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
