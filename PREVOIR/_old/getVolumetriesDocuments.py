# coding: utf-8
import datetime
import glob
import json
import os
import re
import shutil
import sys
import time
import zipfile
from datetime import date
from os.path import *
from xml.dom.minidom import parseString
from xml.parsers.expat import ExpatError
from rich.progress import Progress, TimeElapsedColumn, SpinnerColumn

import AOG.logfile as logfile
import AOG.logvolumetries as volumetries
import AOG.pdftools as pdftools
import AOG.sendmailtools as sendmailtools
import AOG.transferttools as transferttools
from TF.api_request import *

with open(os.path.join(os.path.dirname(__file__), '../appsettings.json')) as json_data:
    config = json.load(json_data)

debug = config["debug"]
if debug is False:
    logInfo = config["logInfo"]
else:
    logInfo = "debug"

rep_pdf = config["dirs"]["rep_pdf"]

rep_adm = os.path.join(rep_pdf, config["dirs"]["rep_adm"])
rep_grdt = os.path.join(rep_adm, config["dirs"]["rep_grdt"])

repLotBlanc = os.path.join(rep_adm, config["dirs"]["rep_lot_Blanc"])

rep_flotte = os.path.join(rep_pdf, config["dirs"]["rep_flotte"])

rep_constructeurs = os.path.join(rep_pdf, config["dirs"]["rep_constructeurs"])

rep_grdt_auto = os.path.join(rep_grdt, config["dirs"]["rep_grdt_auto"])
rep_grdt_auto_tempo = os.path.join(rep_grdt, config["dirs"]["rep_grdt_auto_tempo"])
rep_grdt_ccar = os.path.join(rep_grdt, config["dirs"]["rep_grdt_ccar"])
rep_grdt_cyclo50 = os.path.join(rep_grdt, config["dirs"]["rep_grdt_cyclo50"])
rep_grdt_moto = os.path.join(rep_grdt, config["dirs"]["rep_grdt_moto"])
rep_grdt_vsp = os.path.join(rep_grdt, config["dirs"]["rep_grdt_vsp"])

ENV = config['ENV']
if ENV == "PROD":
    dirout = os.path.join(rep_grdt, config["dirs"]["outputdirPREVOIR"])
else:
    dirout = os.getcwd()

#################################### DECLARATION FONCTIONS ####################################
class docToPrevoir():
    def __init__(self):
        # logger
        self.log = logfile.Logfile(name="impressionprevoir")
        self._logmode = logInfo

        if self._logmode in ["info", "warn", "debug", "error"]:
            self.logwrite = self.log.create_logger(self._logmode, True)
        else:
            self.logwrite = self.log.create_logger("info", True)

        self.rep_travail = os.path.join(os.getcwd(), "travail")
        if os.path.isdir(self.rep_travail) is True:
            shutil.rmtree(self.rep_travail)
        os.makedirs(self.rep_travail)
        self.listedocs = []

    def delete(self):
        shutil.rmtree(self.rep_travail)
        del self.log
        del self.logwrite

    # retour date fichier
    def getFileTimeStamp(self, doc):
        ts = time.gmtime()
        return time.strftime("%Y%m%d", time.localtime(os.path.getmtime(doc)))

    def addDocToList(self, docPath, typedoc):
        try:
            doc = {}
            doc['path'] = docPath
            doc['type'] = typedoc
            doc['name'] = os.path.basename(docPath)
            doc['pages'] = pdftools.getnbpagespdf(docPath)
            doc['contrat'] = self.getRefContrat(docPath)
            # doc['branche'] = get_branche()
            self.logwrite.debug("ajout de " + str(doc))
            self.listedocs.append(doc)
        except Exception as ex:
            self.logwrite.warning("Erreur de lecture pour le fichier : " + str(docPath) + " | " + str(ex))

    # @param
    #       date String Date d'un jour egal ou anterieur a aujourd'hui
    # @Traitement
    #       Liste l'ensemble des documents a imprimer du jour passe en argument
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def listDoc_Jour(self, date):
        self.logwrite.info("Debut liste doc du jour : " + date)
        listeDocFlotte = []
        listeDocGRDT = []
        listeDocConstructeurs = []
        listeDocLotBlanc = []
        listeDocFlotte = dtp.listDocJour_Flotte(date)
        listeDocGRDT = dtp.listDocJour_GRDT(date)
        listeDocConstructeurs = dtp.listDocJour_Constructeur(date)
        listeDocLotBlanc = dtp.listDocJour_LotBLANC(date)
        listDocJour = listeDocFlotte + listeDocGRDT + listeDocConstructeurs + listeDocLotBlanc
        return listDocJour

    # @param
    #       date String Date format d'un jour egal ou anterieur a aujourd'hui, dit le jour sans fin
    # @Traitement
    #       Liste l'ensemble des documents à imprimer pour la Flotte du jour passe en argument
    #       On parcourt les documents en sélectionnant ceux du jour dans les sous rep suivants :
    #           - cv : fichiers commençant par "CVDEFINITIVE"
    #           - contrats : fichiers commençant par "CP-FLOTTE-"
    #       N.B : les docs sont nommées de la sorte :
    #           - cv : CVDEFINITIVE-FLOTTE-04F101661-01-1-20210928-092548.PDF
    #           - contrats : CP-FLOTTE-04F101735-01.PDF ; CP-FLOTTE-04F100955-02-20210929-095531-DUPLICATA.PDF
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def listDocJour_Flotte(self, date):
        self.logwrite.info("Debut liste doc du jour de la flotte : " + date)
        listdocFlotte = []

        repFlotteCV = rep_flotte + "\\cv"
        repFlotteCP = rep_flotte + "\\contrats"

        # PARTIE VERIF CV
        self.logwrite.info("repFlotteCV = " + repFlotteCV)
        typeDoc = "FlotteCV"
        # 19/08/22 : evolution avec mise en place de la date dans la recherche pour ameliorer les temps de performances
        # 19/08/22 : en analyse, pas de cas où le fichier est mal date
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task("        Progression ", total=len(glob.glob(repFlotteCV + '\\*-' + date + '-*')))
            for docCV in glob.glob(repFlotteCV + '\\*-' + date + '-*'):
                progress.advance(task)
                # on ne garantit jamais que l'entité est un fichier, qui sait qu'un excentrique a mis un sous repertoire
                if os.path.isfile(docCV):
                    if self.getFileTimeStamp(docCV) == date:
                        pattern = re.compile("CVFACS.*")
                        # Si ce n'est pas une facsimile
                        # /!\ ATTENTION - cette partie peut poser problème si
                        # les facsimiles deviennent des attestations --- A VOIR
                        if not pattern.match(basename(docCV)):
                            listdocFlotte.append(docCV)
                            self.addDocToList(docCV, typeDoc)

        self.logwrite.info("")
        # PARTIE VERIF CP
        typeDoc = "FlotteCP"
        self.logwrite.info("repFlotteCP = " + repFlotteCP)
        # on ne prend que les cp - d'apres mes verifications le nom de tous les pdf de CP commencent par "CP"
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task("        Progression ", total=len(glob.glob(repFlotteCP + '\\CP*')))
            for docCP in glob.glob(repFlotteCP + '\\CP*'):
                progress.advance(task)

                # comme d'hab par mesure de securite on verifie que ca soit bien un fichier
                if os.path.isfile(docCP):
                    # verification de la date
                    if self.getFileTimeStamp(docCP) == date:
                        self.addDocToList(docCP, typeDoc)
                        listdocFlotte.append(docCP)

        self.logwrite.info("")
        return listdocFlotte

    # @param
    #       date String Format AAAAMMJJ Date d'un jour egal ou anterieur a aujourd'hui
    # @Traitement
    #       Liste l'ensemble des documents a imprimer pour GRDT du jour passe en argument
    #       Dans le REP GRDT ( Constante "rep_grdt" )
    #           parcourir l'ensemble des sous repertoires ( AUTO, AUTO_TEMPO, CCAR, CYCLO, MOTO, VSP )
    #           dans chaque sous repertoires, aller dans le repertoire de la date du jour " type AAAAMMJJ"
    #           Lister l'ensemble des docs des sous repertoires presentS dans le rep de la date du jour
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def listDocJour_GRDT(self, date):
        self.logwrite.info("Debut liste doc du jour des branches GRDT : " + date)
        listDocGRDT = []
        typeDoc = "GRDT"
        self.logwrite.info("rep_grdt = " + rep_grdt)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task("        Progression ", total=len(glob.glob(rep_grdt + '\\*')))
            for repBranche in glob.glob(rep_grdt + '\\*'):
                progress.advance(task)
                # on verifie que c'est bien un repertoire
                # exemple \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO
                if os.path.isdir(repBranche):
                    repBrancheDate = os.path.join(repBranche, date)
                    # on verifie l'existance de ce repertoire date
                    # exemple :  \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO\20210929
                    if os.path.isdir(repBrancheDate):
                        # on boucle sur chacune des ressource du repertoire date
                        for repBrancheDate_repTypeDoc in glob.glob(repBrancheDate + '\\*'):
                            # on verifie si la ressource est un repertoire et donc un repertoire de document
                            # exemple bon :  \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO\20210929\CP
                            # exemple faux : \\10.128.3.120\cortex\prod\pdf\ADM\GRDT\AUTO\20210929\CV_AUTO.pdf
                            if os.path.isdir(repBrancheDate):
                                # on parcours le sous repertoire de document en listant les doc pdf
                                for doc in glob.glob(repBrancheDate_repTypeDoc + '\\*'):
                                    # on s'assure que ça soit quand meme un fichier, par acquis de conscience
                                    if os.path.isfile(doc):
                                        # et on ajoute le doc dans la liste
                                        listDocGRDT.append(doc)
                                        self.addDocToList(doc, typeDoc)

        self.logwrite.info("")
        return listDocGRDT

    # @param
    #       date String Date d'un jour egal ou anterieur a aujourd'hui
    # @Traitement
    #       Liste l'ensemble des documents a imprimer pour les Constructeurs du jour passe en argument
    #       Dans le sous rep "rep_constructeurs" selection le rep date AAAAMMJJ
    #       Dans le rep date / AUTO , prendre l'ensemble des documents des sous repertoires
    #       ( exemple : ECHEANCIER, devis, cv, CP, AVENANT )
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def listDocJour_Constructeur(self, date):
        self.logwrite.info("Debut liste doc du jour des Constructeurs : " + date)
        listDocConstructeurs = []
        repConstructDate = os.path.join(rep_constructeurs, date, "AUTO")
        # Dans ce repertoire il y a differents sous repertoire par type de doc
        # il y a aussi des pdfs qui sont realisee apres fusion.
        # On doit parcourir uniquement les sous repertoires pour avoir les pdf unitaires
        typeDoc = "Constructeurs"
        self.logwrite.info("repConstructDate = " + repConstructDate)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(),) as progress:
            task = progress.add_task("        Progression ", total=len(glob.glob(repConstructDate + '\\*')))
            for repTypeDoc in glob.glob(repConstructDate + '\\*'):
                progress.advance(task)
                if os.path.isdir(repTypeDoc):
                    # Si c'est bien un repertoire on le parcourt et on liste l'ensemble des fichiers contenus
                    for doc in glob.glob(repTypeDoc + '\\*'):
                        if os.path.isfile(doc):
                            listDocConstructeurs.append(doc)
                            self.addDocToList(doc, typeDoc)
        self.logwrite.info("")
        return listDocConstructeurs

    # @param
    #       AUCUN
    # @Traitement
    #       On va chercher dans le répertoire de prod où l'ensemble des documents pdf est depose avant fusion
    # @Return
    #       String[] Liste de liens absolus de documents dans un tableau / array
    def listDocJour_LotBLANC(self, date):
        self.logwrite.info("Debut liste doc du jour des lots blancs (hors GRDT/FLOTTE/CONSTRUCTEURS) : " + date)
        listDocBlanc = []

        # self.logwrite.info(repLotBlanc)
        typeDoc = "BLANC"
        self.logwrite.info("repLotBlanc = " + repLotBlanc)
        datecompare = str(date[0:4]) + "-" + str(date[4:6]) + "-" + str(date[6:8])
        for dirs in os.listdir(repLotBlanc):
            if datecompare == dirs:
                # print(dirs)
                dir = os.path.join(repLotBlanc, dirs)
                self.logwrite.info("repLotBlanc = " + dir)
                with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
                    task = progress.add_task("        Progression ", total=len(os.listdir(repLotBlanc)))
                    # for doc in glob.glob(repLotBlanc+'\\*'):
                    for docs in os.listdir(dir):

                    # for docs in os.listdir(repLotBlanc):
                        progress.advance(task)
                        # on verifie que c'est bien un repertoire
                        # exemple \\10.128.3.120\cortex\prod\pdf\ADM\DEMANDE_IMPRESSION\prevoir
                        # on s'assure que ça soit quand meme un fichier, par acquis de conscience
                        doc = os.path.join(repLotBlanc, docs)
                        if os.path.isfile(doc):
                            # 19/08/22 : Maintenance à chaud = usine... a faire evoluer pour rendre plus propre
                            if basename(doc) != 'Thumbs.db':
                                # et on ajoute le doc dans la liste
                                listDocBlanc.append(doc)
                                self.addDocToList(doc, typeDoc)

        self.logwrite.info("")
        return listDocBlanc

    # @param
    #       chemin_fichier_impression String chemin absolu du pdf a imprimer
    # @Traitement
    #       Recuperation du nom du fichier a partir du chemin absolu
    # @Return
    #    String nom du document
    def getNomDoc(self, cheminFichierImpression):
        return os.path.split(cheminFichierImpression)[-1]

    # @param
    #       nom_fichier_impression String chemin / nom du pdf a imprimer
    # @Traitement
    #       Tous les pdf, a l'heure actuelle sont nommes de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String la reference contrat
    def getRefContrat(self, nomFichierImpression):
        pattern = re.compile("^([^-]+-[^-]+)-([^-]+-[^-]+)(-[^-]+)*\.[pdf|PDF]")
        # On dirait des smileys
        nomFichier = basename(nomFichierImpression)
        matcher = pattern.match(nomFichier)
        return matcher.group(2)

    # @param
    #       ref_contrat String type num_contrat - ref piece
    # @Traitement
    #       Récupération du numéro de contrat
    #       Tous les pdf, a l'heure actuelle sont nommes de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String le numero du contrat
    def getNumContrat(self, refContrat):
        regexPattern = '^([^-]+)-([^-]+)'
        if re.match(regexPattern, refContrat) is not None:
            pattern = re.compile(regexPattern)
            matcher = pattern.match(refContrat)
        return matcher.group(1)

    # @Return
    # string le numero de contrat
    # dans le cas de la branche flotte, on peut avoir un contrat mais plusieurs immatricules, on recupere le compteur
    def getcounter(self, nomFichierImpression):
        regexPattern = re.compile("^([^-]+-[^-]+)-([^-]+-[^-]+)-((100)|(0*\d{1,2}))(-[^-]+)*\.[pdf|PDF]")
        if re.match(regexPattern, nomFichierImpression) is not None:
            pattern = re.compile(regexPattern)
            matcher = pattern.match(nomFichierImpression)
            return matcher.group(3)
        else:
            return None

    # @param
    #       ref_contrat String type num_contrat - ref piece
    # @Traitement
    #       Idealement il faudrait une verification en base pour verifier
    #       qu'il ne s'agisse pas d'une enieme demande de generation mais
    #       on ne peut pas affirmer que tout soit trace en base car c'est un fait
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #       Boolean    True si ref piece == "01"
    def isAffaireNouvelle(self, refContrat):
        regexPattern = '^([^-]+)-([^-]+)'
        if re.match(regexPattern, refContrat) is not None:
            pattern = re.compile(regexPattern)
            matcher = pattern.match(refContrat)
            if matcher.group(2) == "01":
                return True
        return False

    def isduplicata(self, nomFichierImpression):
        regexPattern = re.compile("(^[^-]).*-(DUPLICATA)\.[pdf|PDF]")
        if re.match(regexPattern, nomFichierImpression) is not None:
            pattern = re.compile(regexPattern)
            matcher = pattern.match(nomFichierImpression)
            return matcher.group(2)
        else:
            return ""

    # @param
    #       nom_fichier_impression String nom du pdf à imprimer
    # @Traitement
    #       Tous les pdf, a l'heure actuelle sont nommés de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION : il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String la Branche
    def getBranche(self, nomFichierImpression):
        pattern = re.compile("^([^-]+)-([^-]+)-([^-]+-[^-]+)(-[^-]+)*\.[pdf|PDF]")
        # On dirait des smileys
        nomFichier = basename(nomFichierImpression)
        matcher = pattern.match(nomFichier)
        return matcher.group(2)

    # @param
    #       nom_fichier_impression String nom du pdf a imprimer
    # @Traitement
    #       Tous les pdf, a l'heure actuelle sont nommes de la sorte :
    #       [type_doc]-[Branche]-[num_contrat]-[numPiece] + [autres]* + .[pdf|PDF]
    #       ATTENTION :  il ne faut pas utiliser cette fonction avec les devis
    # @Return
    #    String Type du document
    def getTypeDoc(self, nomFichierImpression):
        pattern = re.compile("^([^-]+)-([^-]+)-([^-]+-[^-]+)(-[^-]+)*\.[pdf|PDF]")
        # On dirait des smileys
        nomFichier = basename(nomFichierImpression)
        matcher = pattern.match(nomFichier)
        return matcher.group(1)


    # @param
    #       liste_doc_unique    String[] Liste de document a transferer
    # @Traitement
    #       Transfert les documents à imprimer dans le repertoir prevoir
    # @Return
    #       String[] Liste de liens absolus des documents dans un tableau / array
    def transfertListeDoc(self, listeDocUnique, rep_cible):
        listDocsTransferes = []
        listDocsNonTransferes = []
        self.logwrite.info("Debut transfert de fichier vers " + rep_cible)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(),) as progress:
            task = progress.add_task("        Progression ", total=len(listeDocUnique))
            for docUnique in listeDocUnique:
                progress.advance(task)
                # Cas d'exclusion des documents à ne pas imprimer
                dontprint = False
                for item in ["CVFACS", "ATTPROV"]:
                    if item in docUnique:
                        dontprint = True

                if dontprint is True:
                    listDocsNonTransferes.append(docUnique)
                    continue

                try:
                    if isinstance(docUnique, str):
                        cheminFichierTransfere = shutil.copy(docUnique, os.path.join(rep_cible, self.getNomDoc(docUnique)))
                    else:
                        cheminFichierTransfere = shutil.copy(docUnique['path'],
                                                             os.path.join(rep_cible, self.getNomDoc(docUnique['name'])))
                    #print(cheminFichierTransfere)
                    listDocsTransferes.append(cheminFichierTransfere)
                except shutil.SameFileError as error:
                    self.logwrite.warning(
                        "Erreur : c'est le même chemin pour le fichier entre la source et la destination.")
                    self.logwrite.warning(error)
                    listDocsNonTransferes.append(cheminFichierTransfere)
                except PermissionError as error:
                    self.logwrite.warning("Erreur : Permission denied.")
                    self.logwrite.warning(error)
                    listDocsNonTransferes.append(cheminFichierTransfere)
                except Exception as ex:
                    self.logwrite.warning("Error occurred while copying file.")
                    self.logwrite.warning(str(ex))
                    listDocsNonTransferes.append(cheminFichierTransfere)
        self.logwrite.info("Fin transfert de fichier vers " + rep_cible)
        return listDocsTransferes, listDocsNonTransferes

    # @param
    #       num_contrat    String[] numero du contrat
    # @Traitement
    #       Extraction des donnees 'marque apporteur' et 'numéro de courtier' du flux XML retourne par l API
    # @Return
    #       Dict{}    Dictionnaire contenant deux couples clé:valeur avec les cles marqueApporteur et numCourtier
    def getDonneesMiseSousPli(self, numContrat, TypeDoc):
        donneesMiseSousPli = dict()
        # Recuperation des donnees via l API
        donneesXML = get_response_xml(numContrat, TypeDoc, self.logwrite)
        # On teste si on a un xml vide ou non

        if donneesXML != None and donneesXML != "":

            # Creation du parseur et validation du flux XML
            parser = parseString(donneesXML)
            # On instancie le Document DOM
            dataDocuments = parser.getElementsByTagName('DataDocumentViewModel')
            for dataDocument in dataDocuments:
                # On verifie si le la balise 'NomApporteur' contient l'attribut 'i:nil' qui signifie qu elle est vide
                # if dataDocument.getElementsByTagName('NomApporteur')[0].hasAttribute('i:nil') == True:
                #    marqueApporteur = 'VIDE'
                #    numCourtier = dataDocument.getElementsByTagName('CodeCourtier')[0].firstChild.data
                #    donneesMiseSousPli = {'marqueApporteur': marqueApporteur, 'numCourtier': numCourtier}
                # else:
                #    marqueApporteur = dataDocument.getElementsByTagName('NomApporteur')[0].firstChild.data
                ReferenceContrat = numContrat
                numCourtier = dataDocument.getElementsByTagName('CodeCourtier')[0].firstChild.data

                # On verifie si le la balise 'numCourtier' contient l'attribut 'i:nil' qui signifie qu elle est vide
                if dataDocument.getElementsByTagName('CodeCourtier')[0].hasAttribute('i:nil') != True:
                    marqueApporteur = numCourtier[:4]
                if dataDocument.getElementsByTagName('ReferenceContrat')[0].hasAttribute('i:nil') != True:
                    ReferenceContrat = dataDocument.getElementsByTagName('ReferenceContrat')[0].firstChild.data

        else:
            if TypeDoc == "SIN":
                ReferenceContrat = numContrat + "-00"
            else:
                ReferenceContrat = numContrat
            marqueApporteur = "VIDE"
            numCourtier = "VIDE"

        donneesMiseSousPli = {'marqueApporteur': marqueApporteur,
                              'numCourtier': numCourtier,
                              'ReferenceContrat': ReferenceContrat}
        return donneesMiseSousPli

    # @param
    #       liste_doc_unique_arenommer    String[] Liste des documents à imprimer tranferes dans le repertoire Prevoir
    # @Traitement
    #       Renommage de tous les fichiers PDF comme suit SAUF DEVIS :
    #       [TYPE_DOC]-[BRANCHE]-[N°_CONTRAT]-[N°_PIECE]-[MARQUE_APPORTEUR]-[N°_COURTIER]-[DATE]
    #       Si on est le cas type document = DEVIS :
    #       on garde la nomenclature existante : [DEVIS]-[BRANCHE]-[NUMERO_DEVIS]-[MARQUE_APPORTEUR]-[N°_COURTIER]-[DATE]
    # @Return
    #       String[] Liste de liens absolus des documents dans un tableau / array
    def renommageFichiersImpression(self, listeDocUniqueArenommer):
        listFichiersRenommes = []
        listNomCVPROVISOIRE = ["CVERTE_PRO"]
        listNomCVDEFINITIVE = ["CV", "CVD", "CV_REMORQUE_AXA", "CV_REMORQUE_EQUITE", "CARTES_VERTES", "CARTE_VERTE"]
        duplicata = ""
        # for chemin_fichier_impression in liste_doc_unique_arenommer:
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(),) as progress:
            task = progress.add_task("        Progression ", total=len(self.listedocs))
            for fichier in self.listedocs:
                progress.advance(task)
                cheminFichierImpression = fichier["path"]
                nomFichierImpression = self.getNomDoc(cheminFichierImpression)
                nomFichierImpressionInfo = nomFichierImpression.split("-")
                duplicata = self.isduplicata(cheminFichierImpression)

                # CAS CHIEN-CHAT pour DEVIS/CONTRAT : La branche CHIEN-CHAT avec le "-" comme split provoque une
                # separation non souhaitée par rapport aux autres cas On va donc conditionner pour avoir une liste
                # conforme par rapport aux autres cas
                if nomFichierImpressionInfo[1] == "CHIEN" and nomFichierImpressionInfo[2] == "CHAT":
                    # on renomme, pour le nom de la branche, le 2e element : nomFichierImpressionInfo[1] = "CHIENCHAT"
                    nomFichierImpressionInfo[1] = nomFichierImpressionInfo[1] + nomFichierImpressionInfo[2]
                    # on corrige donc la liste en supprimant le 3e element : nomFichierImpressionInfo[2] = "CHAT" pour
                    # avoir l'element suivant i.e le num CONTRAT/DEVIS/SIN
                    del nomFichierImpressionInfo[2]
                    nomFichierImpression = nomFichierImpression.replace("CHIEN-CHAT", "CHIENCHAT")

                # CAS DUPLICATA, on rajoute la mention
                if duplicata != "":
                    duplicata = "-" + duplicata

                # CAS DEVIS
                if nomFichierImpressionInfo[0] == "DEVIS":
                    typeDoc = "DEVIS"
                    branche = nomFichierImpressionInfo[1]
                    numDevis = nomFichierImpressionInfo[2]
                    # le numero DEVIS ne fonctionne pas actuellement avec editique-API et ne l'utilise pas
                    # marqueApporteur = donneesAPI["marqueApporteur"]
                    # numCourtier = donneesAPI["numCourtier"]
                    fichier_NouveauNom = typeDoc + "-" + branche + "-" + numDevis + "-" + "VIDE" + "-" + "VIDE" + "-" + date + duplicata

                # CAS Documents SINISTRE/INDEMNISATION (type doc commencant par SIN)
                elif nomFichierImpressionInfo[0][:3] == "SIN":
                    typeDoc = nomFichierImpressionInfo[0]
                    branche = nomFichierImpressionInfo[1]
                    numSin = nomFichierImpressionInfo[2]
                    # le numero sinistre fonctionne depuis 07/22 avec editique-API
                    try:
                        donneesAPI = self.getDonneesMiseSousPli(numSin, "SIN")
                    except ExpatError as error:
                        self.logwrite.warning("Erreur sur le fichier : " + cheminFichierImpression)
                        self.logwrite.warning(str(error))
                    except Exception as ex:
                        self.logwrite.warning("Unexpected error : " + str(ex))

                    marqueApporteur = donneesAPI["marqueApporteur"]
                    numCourtier = donneesAPI["numCourtier"]
                    refContrat = donneesAPI["ReferenceContrat"]

                    fichier_NouveauNom = typeDoc + "-" + branche + "-" + refContrat + "-" + marqueApporteur + "-" + numCourtier + "-" + date + "-" + count + duplicata

                # CAS GENERAL
                else:
                    typeDoc = self.getTypeDoc(nomFichierImpression)
                    branche = self.getBranche(nomFichierImpression)
                    refContrat = self.getRefContrat(nomFichierImpression)
                    numContrat = self.getNumContrat(refContrat)
                    try:
                        donneesAPI = self.getDonneesMiseSousPli(numContrat, "")
                    except ExpatError as error:
                        self.logwrite.warning("Erreur sur le fichier : " + cheminFichierImpression)
                        self.logwrite.warning(str(error))
                    except Exception as error:
                        self.logwrite.warning("Unexpected error : " + str(error))

                    marqueApporteur = donneesAPI["marqueApporteur"]
                    numCourtier = donneesAPI["numCourtier"]
                    count = "0"
                    if branche == "FLOTTE":
                        if typeDoc == "CVPROV":
                            typeDoc = "ATTPROV"
                        if self.getcounter(nomFichierImpression) is not None:
                            count = "-" + self.getcounter(nomFichierImpression)
                    if typeDoc in listNomCVDEFINITIVE:
                        typeDoc = "CVDEFINITIVE"
                    if typeDoc in listNomCVPROVISOIRE:
                        typeDoc = "CVPROV"

                    fichier_NouveauNom = typeDoc + "-" + branche + "-" + refContrat + "-" + marqueApporteur + "-" + numCourtier + "-" + date + "-" + count + duplicata

                cheminNouveauFichierImpression = os.path.join(self.rep_travail, fichier_NouveauNom + ".pdf")

                self.logwrite.debug(
                    "   Old name : " + os.path.basename(cheminFichierImpression))
                self.logwrite.debug("   New name : " + os.path.basename(cheminNouveauFichierImpression))

                try:
                    os.rename(os.path.join(self.rep_travail, os.path.basename(cheminFichierImpression)),
                              cheminNouveauFichierImpression)
                    listFichiersRenommes.append(cheminNouveauFichierImpression)
                except FileExistsError as error:
                    # exemple:
                    # CVPROV-AUTO-04M37970-01-20220406-120757
                    # CVPROV-AUTO-04M37970-01-20220406-122107
                    # Ce sont des numeros de contrat identique mais l heure de creation est differente
                    # Hors dans le renommage, on omet l heure de creation
                    # Des tests effectues, on constate que ce sont des pdfs avec des contenus identiques
                    self.logwrite.debug(
                        "suppression du doublon : " + cheminFichierImpression)
                    os.remove(cheminFichierImpression)
                except FileNotFoundError as error:
                    # On recherche si le fichier n a pas deja ete renomme
                    if cheminNouveauFichierImpression in listFichiersRenommes:
                        self.logwrite.debug("Le fichier existe deja " + cheminNouveauFichierImpression)
                        continue

        return listFichiersRenommes

    def calculVolumetries(self, listeDocUniqueArenommer, dateTrt):
        try:
            year = int(dateTrt[0:4])
            month = int(dateTrt[4:6])
            day = int(dateTrt[6:8])

            voldocs = {}
            volconsommables = {}
            # Récupération des volumes pour chaque type de documents
            for file in listeDocUniqueArenommer:
                if file != "Thumbs.db":
                    items = os.path.basename(file).split("-")
                    # (items)
                    typedoc = items[0]
                    branche = items[1]
                    numcontrat = items[2]
                    numpiece = items[3]
                    marque = items[4]
                    courtier = items[5]
                    self.logwrite.debug(file)
                    self.logwrite.debug("    " + str(items))
                    nbPagesDoc = pdftools.getnbpagespdf(file)
                    if typedoc in voldocs.keys():
                        voldocs[typedoc]['nb_docs'] = voldocs[typedoc]['nb_docs'] + 1
                        voldocs[typedoc]['nbPages'] = voldocs[typedoc]['nbPages'] + nbPagesDoc
                    else:
                        item = {'nb_docs': 1, 'nbPages': nbPagesDoc}
                        voldocs[typedoc] = item

                    if marque[0:2] == "NE":
                        marque = "NETVOX"
                    elif marque in ["FORD", "OPEL", "VOLV", "BMWA", "MINI", "VIDE"]:
                        marque = "NEUTRE"

                    # Calcul des consommables
                    if typedoc in ['CVDEFINITIVE', 'CVPROV', 'ENVOI_PV_CONSTAT'] and branche in ['MOTO', 'CYCLO']:
                        #print("Porte Vignette 2 roues")
                        consommable = "PRV_PV2R"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}
                    elif typedoc == "CP" and numpiece == "01" and branche in ['AUTO' ,'CCAR', 'VSP'] and marque != "GRDT":
                        #print("Porte Vignette 4 roues " + marque)
                        consommable = "PRV_PV4R-" + marque
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}
                    elif typedoc == "ENVOI_PV_CONSTAT" and branche in ['AUTO' ,'CCAR', 'FLOTTE']:
                        #print("Porte Vignette 4 roues " + marque)
                        consommable = "PRV_PV4R-" + marque
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}
                    elif typedoc == "CVDEFINITIVE" and branche == 'FLOTTE':
                        #print("Porte Vignette 4 roues " + marque)
                        consommable = "PRV_PV4R-" + marque
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}
                    elif typedoc == "CVDEFINITIVE" and branche in ['AUTO', 'VSP'] and marque == "GRDT":
                        #print("Porte Vignette 4 roues " + marque)
                        consommable = "PRV_PV4R-AOL"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}

                    if typedoc == "CP" and numpiece == "01" and branche in ['AUTO' ,'CYCLO', 'MOTO', 'CCAR', 'FLOTTE', 'VSP']:
                        #print("Constat Amiable Auto")
                        consommable = "PRV_CONSTAT-AUTO"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}

                    if typedoc == "CP" and numpiece == "01" and branche == "MRH":
                        #print("Constat Amiable Degat des Eaux")
                        consommable = "PRV_CONSTAT-EAU"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}
                    elif typedoc == "ENVOI_PV_CONSTAT" and branche == "MRH":
                        #print("Constat Amiable Degat des Eaux")
                        consommable = "PRV_CONSTAT-EAU"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}

                    # Enveloppes
                    if nbPagesDoc > 8:
                        consommable = "PRV_ENV-C4"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}
                    else:
                        consommable = "PRV_ENV-C5"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}

                    # Pages Blanches / Carte Verte
                    if typedoc in ['CVDEFINITIVE', 'CVPROV']:
                        consommable = "PRV_PAPIER-CV"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + 1
                        else:
                            volconsommables[consommable] = {'unites': 1}
                    else:
                        consommable = "PRV_PAPIER-BLANC"
                        if consommable in volconsommables.keys():
                            volconsommables[consommable]['unites'] = volconsommables[consommable]['unites'] + nbPagesDoc
                        else:
                            volconsommables[consommable] = {'unites': nbPagesDoc}


            lv = volumetries.Volumetries("CONSOMMABLES", self.logwrite, env=ENV)
            for item in volconsommables:
                for i in range(3, 20):
                    #print(item)
                    # L'entrée est trouvée, on renseigne la valeur
                    if item == lv.ws.cell(i, 2).value:
                        # Décalage du quantième du jour et placement de la bonne valeur
                        val = lv.ws.cell(i, 3 + year - 2022 + month - 1).value
                        if val is None:
                            newval = volconsommables[item]['unites']
                        else:
                            newval = int(lv.ws.cell(i, 3 + year - 2022 + month - 1).value) + int(volconsommables[item]['unites'])
                        lv.ws.cell(i, 3 + year - 2022 + month - 1).value = newval
                        self.logwrite.debug(
                            item + " - " + str(i) + ":" + str(3 + 3 + year - 2022 + month - 1) + " - " + str(newval))
                        break

                    # L'entrée n'existe pas, on la crée
                    elif lv.ws.cell(i, 2).value is None:
                        #print("ee")
                        lv.ws.cell(i, 2).value = item
                        lv.ws.cell(i, 3 + year - 2022 + month - 1).value = volconsommables[item]['unites']
                        self.logwrite.debug(
                            item + "-" + str(i) + ":" + str(3 + year - 2022 + month - 1) + " - " + str(volconsommables[item]['unites']))
                        break
            lv.closefile()
            return(voldocs, volconsommables)
        except Exception as ex:
            print(str(ex))
            sys.exit(0)

    def purge(self, rep):
        self.logwrite.info("Purge de " + rep)
        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task("        Progression ", total=len(glob.glob(rep + '\\*')))
            for files in glob.glob(rep + '\\*'):
                progress.advance(task)
                # on ne garantit jamais que l'entité est un fichier, qui sait ce qu'un excentrique a mis un sous repertoire
                if os.path.isfile(files):
                    os.remove(files)
                    self.logwrite.debug(files)
        self.logwrite.info("Purge terminée")


    def _isExclusion(self, docname, liste):
        for i in range(0, 2):
            if docname.split("-")[i] in liste:
                return True
        return False


    def compression(self, repDuJourLotBlanc, dateDuJour):
        nbDocs = 0
        nbDocsErr = 0
        #listexclusion = []
        lsiteDocErr = []

        #exclusionlist = open(os.path.join(os.path.dirname(__file__), 'nepasimprimer.lst'), 'r')
        #for lines in exclusionlist:
        #    listexclusion.append(lines.strip())
        listexclusion = config["validationimpression"]
        dtp.logwrite.info("    Validation des impressions pour : " + str(listexclusion))

        zipnamePREVOIR = os.path.join(repDuJourLotBlanc, "ASSURONE_" + dateDuJour + ".zip")
        zipPREVOIR = zipfile.ZipFile(zipnamePREVOIR, "w")
        self.logwrite.info("  Ouverture du fichier zip : " + zipnamePREVOIR)

        zipnameERR = os.path.join(repDuJourLotBlanc, "ERR_" + dateDuJour + ".zip")
        zipERR = zipfile.ZipFile(zipnameERR, "w")
        self.logwrite.info("  Ouverture du fichier zip d'exclusion : " + zipnameERR)

        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(), ) as progress:
            task = progress.add_task("    Compression ", total=(len(os.listdir(repDuJourLotBlanc))))
            for file in os.listdir(repDuJourLotBlanc):
                progress.advance(task)
                filePath = os.path.join(repDuJourLotBlanc, file)
                if file.endswith(".pdf") is True:
                    if self._isExclusion(file, listexclusion) is False:
                        self.logwrite.debug("      Ajout du fichier : " + file)
                        zipPREVOIR.write(filePath, os.path.basename(filePath))
                        os.remove(filePath)
                        nbDocs = nbDocs + 1
                    else:
                        self.logwrite.warning("      Ajout du fichier exclus : " + file)
                        zipERR.write(filePath, os.path.basename(filePath))
                        lsiteDocErr.append(file)
                        os.remove(filePath)
                        nbDocsErr = nbDocsErr + 1

        self.logwrite.info("  Fermeture du fichier zip : " + zipnamePREVOIR)
        self.logwrite.info("  Fermeture du fichier zip : " + zipnameERR)
        if nbDocsErr > 0:
            self.logwrite.info("  il y a " + str(nbDocsErr) + " docs en erreur")
            self.logwrite.info("  envoie d'un mail de recap")
            mailDocError = sendmailtools.Sendmail()
            mailDocError.set_subject("[PREVOIR] Documents non imprimés ce jour")
            body = "Liste des docs en erreur sur la production du jour.\n\n"
            for file in lsiteDocErr:
                body = body + "    " + file + "\n"
            mailDocError.set_body(body)
            mailDocError.set_high_priority()
            mailDocError.add_attachment(zipnameERR)
            mailDocError.go_mail()
        return nbDocs

def creationRepertoireDateDuJour(rep):
    if not os.path.exists(rep):
        os.makedirs(rep)
    return rep


if __name__ == "__main__":
    # J-1 format %Y-%m-%d
    dateprod = datetime.date.today()
    # J-1 Format %Y%m%d
    quantieme = 277
    for i in (range(120, quantieme)):
    #for i in reversed(range(1, 273)):
        date = (dateprod + datetime.timedelta(days=i - quantieme)).strftime("%Y%m%d")
        print(date)


        # date = (dateprod + datetime.timedelta(days=-1)).strftime("%Y%m%d")

        # J Format %Y-%m-%d
        dateDuJour = str(dateprod)

        #################################### DEBUT PROGRAMME ####################################
        dtp = docToPrevoir()
        #mailrecap = sendmailtools.Sendmail()
        try:
            dtp.logwrite.info("Traitement du " + str(dateprod) + " pour les fichiers du {date}".format(date=date))
            dtp.logwrite.info("Mode debug : " + str(debug))
            dtp.logwrite.info("== DEBUT PROGRAMME ==")
            dtp.logwrite.info("")

            # On liste tous les docs des branches : GRDT(AOL)/Flotte/AUTO
            dtp.logwrite.info("== DEBUT LISTE docs des branches : GRDT(AOL)/Flotte/AUTO ==")
            dtp.listDoc_Jour(date)
            dtp.logwrite.info("== FIN LISTE docs des branches : GRDT(AOL)/Flotte/AUTO ==")
            dtp.logwrite.info("")

            # On transfere le lot vers le repertoire de travail pour renommage des fichiers
            dtp.logwrite.info("== DEBUT TRANSFERT REP TRAVAIL ==")
            listeDocUniqueArenommer = []
            listeDocUniqueArenommer = dtp.transfertListeDoc(dtp.listedocs, dtp.rep_travail + "\\")
            dtp.logwrite.info("== FIN TRANSFERT REP TRAVAIL ==")
            dtp.logwrite.info("")

            # On renomme les documents avec l'appel de l'API editique pour les standardiser
            dtp.logwrite.info("== DEBUT RENOMMAGE NOMS VIA API ==")
            listeDocUniqueImpression = []
            listeDocUniqueImpression = dtp.renommageFichiersImpression(listeDocUniqueArenommer)
            dtp.logwrite.info("== FIN RENOMMAGE NOMS VIA API ==")
            dtp.logwrite.info("")

            voldocs = {}
            volconsommables = {}
            voldocs, volconsommables = dtp.calculVolumetries(listeDocUniqueImpression, date)
        except Exception as ex:
            print(str(ex))
            pass
